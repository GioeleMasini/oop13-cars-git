package carsproject.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import carsproject.general.Amanuensis;
import carsproject.general.Coordinates;
import carsproject.map.ITile;
import carsproject.map.MapManager;
import carsproject.map.MapTiles;
import carsproject.vehicles.Garage;
import carsproject.vehicles.IModelVehicle;

/**
 * Contains the object and methods to draw and manage the Map panel and his component
 * 
 * @author Zamagna Marco
 * 
 */

@SuppressWarnings("serial")
public class MapDrawer extends JPanel implements Runnable {
	
			//Loading images of map tiles
		    public static final Image NS = MapDrawer.loadImageFromJar("road.png");
		    public static final Image EW = ImageTool.rotate(MapDrawer.NS, 90);
		    public static final Image INCROCIO = MapDrawer.loadImageFromJar("cross.png");
		    public static final Image GREEN = ImageTool.resize(MapDrawer.loadImageFromJar("grassTexture.jpg"), 40, 40);
		    public static final Image TURN_ES = MapDrawer.loadImageFromJar("bow.png");
		    public static final Image TURN_EN = ImageTool.rotate(MapDrawer.TURN_ES, 90);
		    public static final Image TURN_WN = ImageTool.rotate(MapDrawer.TURN_ES, 180);
		    public static final Image TURN_WS =  ImageTool.rotate(MapDrawer.TURN_ES, 270);
		    public static final Image STOP_N = MapDrawer.loadImageFromJar("stop.png");
		    public static final Image STOP_S = ImageTool.rotate(MapDrawer.STOP_N, 180);
		    public static final Image STOP_E = ImageTool.rotate(MapDrawer.STOP_N, 90);
		    public static final Image STOP_W = ImageTool.rotate(MapDrawer.STOP_N, 270);
		    public static final Image PRIOR_N = MapDrawer.loadImageFromJar("prior.png");
		    public static final Image PRIOR_S = ImageTool.rotate(MapDrawer.PRIOR_N, 180);
		    public static final Image PRIOR_E = ImageTool.rotate(MapDrawer.PRIOR_N, 90);
		    public static final Image PRIOR_W = ImageTool.rotate(MapDrawer.PRIOR_N, 270);
		    public static final Image TL_RED = MapDrawer.loadImageFromJar("TlRed.png");
		    public static final Image TL_GREEN = MapDrawer.loadImageFromJar("TlGreen.png");
		    public static final Image TL_YELLOW = MapDrawer.loadImageFromJar("TlYellow.png");
		    public static final Image[] TREES = new Image[]{loadImageFromJar("tree1.png"), loadImageFromJar("tree2.png")};

		    //Dimensioni della mappa
		    private final int NUM_ROWS;
		    private final int NUM_COLS;
		    
		    public static boolean drawing;
		    
		    private static MapTiles mapTile;
		    
		    private static Double zoom = 0.6;

		    /**
		     * Constructor of {@link MapDrawer} class, create the instance in the mapTile  
		     * and call the  creation of the map in MapManager.
		     * 
		     * @param mapGrid
		     * 			the matrix that contains a string version of the map
		     * 
		     */
		    public MapDrawer(final String[][] mapGrid) {
		    	this.NUM_ROWS = MapManager.getRows();
		    	this.NUM_COLS = MapManager.getColumns();
		    	
		        mapTile = MapTiles.getInstance();
		        
		        //MapManager.createMap(mapGrid);
		        //Setting background of the same color of swing background (something like light-gray)
		        this.setBackground(new Color(0xeeeeee));
		        this.setIgnoreRepaint(true);
		    }

		    /**
		     * Override of JPanel method paintComponent, calls the super method and paint the tile
		     * of a map and create the thread that control the car drawing.
		     * 
		     * @param g
		     * 		graphics of JPanel
		     */
		    
		    @Override
		    public void paintComponent(final Graphics g) {
		        super.paintComponent(g);
		        
		       // g.clearRect(0, 0, getWidth(), getHeight());

		        int x = 0;
                int y = 0;
		        for (int i = 0; i < NUM_ROWS; i++) {
		        	y = i * ITile.TILE_EDGE;
		            for (int j = 0; j < NUM_COLS; j++) {
		            	x = j * ITile.TILE_EDGE;
		                Image image = mapTile.getTileAt(new Coordinates(x, y)).getSprite();

		            	//Zooming
		            	Integer drawY = Math.round(Math.round(i * ITile.TILE_EDGE * MapDrawer.zoom));
		            	Integer drawX = Math.round(Math.round(j * ITile.TILE_EDGE * MapDrawer.zoom));
		            	Integer imgSide = Math.round(Math.round(ITile.TILE_EDGE * MapDrawer.zoom));
		            	
		                g.drawImage(image, drawX, drawY, imgSide, imgSide, null);
		            }
		        }
		        
		        for (IModelVehicle v : Garage.getInstance().getVehicles()) {
		        	if (Coordinates.isLegal(v.getPosition())) {
		        		Image img = ImageTool.rotate(v.getSprite(), Math.toDegrees(v.getCurvatureAngleActual()));
		        		x = v.getPosition().getX() - img.getWidth(null) / 2;
		        		y = v.getPosition().getY() - img.getHeight(null) / 2;
		        		
		        		//Zooming
		        		x = Math.round(Math.round(x * MapDrawer.zoom));
		        		y = Math.round(Math.round(y * MapDrawer.zoom));
		        		Integer imgWidth = Math.round(Math.round(img.getWidth(null) * MapDrawer.zoom));
		        		Integer imgHeight = Math.round(Math.round(img.getHeight(null) * MapDrawer.zoom));
		        		
						g.drawImage(img, x, y, imgWidth, imgHeight, null);
		        	}
				}
		        
		        //Asking a refresh of log panel
		        GUIdrawer.refreshLogPanel();
		    }
		    
		    @Override
			public void run() {
				while (true) {
			        long startTime = System.currentTimeMillis();
					
			        this.repaint();

					long spentTime = System.currentTimeMillis() - startTime;
					try {
	 					Thread.sleep(IModelVehicle.TIME_SLICE - spentTime);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Repaint cannot sleep. Milliseconds value passed as input in Thread.sleep() is negative");
					}
				}
			}
		    
		    /**
		     * Set map zoom. Need a percentage value as double.
		     * (1d for 100% zoom)
		     * 
		     * @param value value of zoom to set
		     */
		    public static void setZoom(Double value) {
		    	MapDrawer.zoom = value;
		    }

		    /**
		     * Return actual zoom value.
		     * 
		     * @return value of the zoom in percentage as double
		     */
		    public static Double getZoom() {
		    	return MapDrawer.zoom;
		    }
		    
		    /**
		     * Return a randomized and flipped/rotated grass image.
		     * 
		     * @return a random image for grass tile
		     */
		    public static Image getGrassImage() {
		    	Integer rand = Math.round(Math.round(Math.random() * 10));
		    	if (rand == 0) {
		    		rand = Math.round(Math.round(Math.random() * (TREES.length - 1)));
		    		Image img = MapDrawer.TREES[rand];
		    		//rotating
		    		rand = Math.round(Math.round(Math.random() * 5));
		    		if (rand == 4) {
		    			//Specchio verticale
		    			img = ImageTool.flipImageVertically(img);
		    		} else if (rand == 5) {
		    			img = ImageTool.flipImageHorizontally(img);
		    		} else {
		    			img = ImageTool.rotate(img, rand * 90);
		    		}
		    		return img;
		    	}
		    	return MapDrawer.GREEN;
		    }
		    
		    /**
		     * Load an image from inside jar, folder "/images".
		     * 
		     * @param name name of the image to load
		     * @return loaded image
		     */
		    public static Image loadImageFromJar(String name) {
		    	return new ImageIcon(MapDrawer.class.getResource("/images/" + name)).getImage();
		    }
}
