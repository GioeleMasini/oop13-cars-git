package carsproject.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileFilter;

import carsproject.general.Amanuensis;
import carsproject.map.ITile;
import carsproject.map.MapManager;
import carsproject.map.MapManager.Point;
import carsproject.map.MapTiles;
import carsproject.map.SpawnController;
import carsproject.vehicles.Garage;
import carsproject.vehicles.IModelVehicle;

/**
 * Contains the object and methods to draw and manage the GUI and his components.
 * 
 * @author Zamagna Marco
 * 
 */


@SuppressWarnings("serial")
public class GUIdrawer extends JFrame {

	private static final String DEFAULT_MAP = "/maps/default.txt";
	private static final String MAP_GUIDE = "/HowToMakeCustomMap.pdf";
	
	private MapDrawer map;
	private static DefaultListModel<String>logVehicleList = new DefaultListModel<String>();
	private JList<String> logVehicle = new JList<String>(logVehicleList);
	
	/**
	 * Constructor of {@link GUIdrawer} class,
	 * create the menu for open the map or load the default map, start and stop the vehicle
	 * and show the credits window.
	 * Obtain the map matrix when the map is create
	 */
	public GUIdrawer() {
		
		
		JMenuBar menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		
		JMenu mapMenu = new JMenu("Map");
		menuBar.add(mapMenu);
		
		JMenu carMenu = new JMenu("Car");
		menuBar.add(carMenu);
		
		JMenu helpMenu = new JMenu("?");
		menuBar.add(helpMenu);
		
		JMenuItem openMap = new JMenuItem("Open");
		openMap.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {
				if (MapTiles.isInstantiated()) {
						//Error dialog
						JOptionPane.showMessageDialog(null, "A map has already been loaded. Can't load another one.", "ERROR", JOptionPane.WARNING_MESSAGE);
				} else {
			        JFileChooser fileChooser = new JFileChooser(".");
			        fileChooser.setFileFilter(new TxtFileFilter());
			        int n = fileChooser.showOpenDialog(GUIdrawer.this);
			        if (n == JFileChooser.APPROVE_OPTION) {
			        	String fileName = fileChooser.getSelectedFile().getAbsolutePath();
			        	String[][] mapGrid = MapManager.loadMap(fileName);
	
				        drawMap(mapGrid);
			        }
		        }
		    }
		});
		
		JMenuItem defaultMap = new JMenuItem("Default");
		defaultMap.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {
				if (MapTiles.isInstantiated()) {
						//Error dialog
						JOptionPane.showMessageDialog(null, "A map has already been loaded. Can't load another one.", "ERROR", JOptionPane.WARNING_MESSAGE);
				} else {
					try {
				        //String mapName = GUIdrawer.class.getResource(DEFAULT_MAP).getPath();
				        String[][] mapGrid = MapManager.loadMap(DEFAULT_MAP);
				        drawMap(mapGrid);
				      } catch (Exception ex) {
				    	  ex.printStackTrace();
				      }
			    }
			}
		});
		mapMenu.add(openMap);
		mapMenu.add(defaultMap);
		
		/* Zoom menu */
		mapMenu.addSeparator();
        JMenu zoomMenu = new JMenu("Zoom");
		mapMenu.add(zoomMenu);
		
		//Creating zoom items from 100% to 50%
		for (int i = 0; i <= 5; i++) {
			final Integer zoomValue = 100 - (i * 10);
			JMenuItem item = new JMenuItem(zoomValue + "%");
			item.addActionListener(new ActionListener() {
				public void actionPerformed(final ActionEvent arg0) {
					MapDrawer.setZoom(new Double(zoomValue) / 100);
				}
			});
			zoomMenu.add(item);
		}
		
		//Exit item
		mapMenu.addSeparator();
		JMenuItem exit = new JMenuItem("Exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {
				System.exit(0);
			}
		});
		mapMenu.add(exit);
		
		JMenuItem spawnNewCar = new JMenuItem("Spawn");
		spawnNewCar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				if (MapTiles.isInstantiated()) {
					if (MapManager.getInstance().getSpawnPoints().isEmpty()) {
						//Error dialog
						JOptionPane.showMessageDialog(null, "No spawn point found. Set them with map configuration file.", "ERROR", JOptionPane.WARNING_MESSAGE);
					} else {
						//Showing window
						SpawnPanel sp = new SpawnPanel();
						Integer result = JOptionPane.showOptionDialog(null, sp, "Spawn new vehicle", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);
						//Checking button pressed
						if (result.equals(JOptionPane.OK_OPTION)) {
							try {
								String selPoint = sp.selPoint.getSelectedItem().toString();
								String selType = sp.selVehicle.getSelectedItem().toString();
								String selTimer = sp.fieldTimer.getText();
								boolean autoStart = sp.chkAutoStart.isSelected();
								//Converting values in those needed
								Integer selTimerInt = selTimer.equals("") ? null : Integer.parseInt(selTimer);
								SpawnController.getInstance().spawnVehicles(selPoint, selType, selTimerInt, autoStart);
							} catch (NumberFormatException ex) {
								//Error dialog
								JOptionPane.showMessageDialog(null, "Timer value not valid. Must be an integer number.", "ERROR", JOptionPane.WARNING_MESSAGE);
							}
						}
					}
				} else {
					//Error dialog
					JOptionPane.showMessageDialog(null, "No map loaded.", "ERROR", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		
		JMenuItem startCars = new JMenuItem("Start all vehicles");
		startCars.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {
				if (MapTiles.isInstantiated()) {
					Garage.getInstance().startAllVehicles();
					GUIdrawer.refreshLogPanel();
				}

			}
		});
		
		JMenuItem stopCars = new JMenuItem("Stop all vehicles");
		stopCars.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {
				Garage.getInstance().stopAllVehicles();
			}
		});
		
		JMenuItem stopTimers = new JMenuItem("Stop all timers");
		stopTimers.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {
				SpawnController.getInstance().stopAllTimers();
			}
		});
		carMenu.add(spawnNewCar);
		carMenu.add(startCars);
		carMenu.add(stopCars);
		carMenu.add(stopTimers);
		
		JMenuItem help = new JMenuItem("Help");
		help.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) { 
				try {
					File file = File.createTempFile("Temporary_Help_User_Guide", ".pdf");  
			        file.deleteOnExit();  
			        OutputStream outStream = new FileOutputStream(file);
			        InputStream inStream = GUIdrawer.class.getResourceAsStream(MAP_GUIDE);
			        
			        try {
			        	int b;
			        	while ((b = inStream.read()) != -1) {
			            outStream.write(b);
			        	}
			        } finally {
			        	inStream.close();
			            outStream.close();  
			        }  
			        java.awt.Desktop.getDesktop().open(file);
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});
		helpMenu.add(help);
		
		JMenuItem about = new JMenuItem("About");
        about.addActionListener(new ActionListener() {
             public void actionPerformed(final ActionEvent arg0) {
                     JDialog d = new JDialog();
                     JPanel panel = new JPanel();
                     panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

                     d.getContentPane().add(panel);
                     JLabel[] labels = new JLabel[]{
                             new JLabel(" "),
                             new JLabel("Ca.R.S.: Cars on the Road Simulator"),
                             new JLabel("Developed by: Masini Gioele, Pruccoli Andrea & Zamagna Marco."),
                             new JLabel("-----------------------------"),
                             new JLabel("Alpha version"),
                             new JLabel(" "),
                             new JLabel("Software distribuited under GNU GENERAL PUBLIC LICENSE."),
                             new JLabel("Code avaiable at bitbucket repository:"),
                             new JLabel("https://bitbucket.org/GioeleMasini/oop13-cars")
                     };
                     for (JLabel l: labels) {
                             l.setAlignmentX(Component.CENTER_ALIGNMENT);
                             panel.add(l);
                     }
                    
                     d.setTitle("Ca.R.S. - About");
                     d.setSize(500, 192);
                     d.setResizable(false);
                     d.setLocationRelativeTo(getParent());
                     d.setVisible(true);
             }
        });
        helpMenu.add(about);
         
		
		JPanel panel = new JPanel();
		panel.setSize(400, this.getHeight());
		panel.setPreferredSize(new Dimension(300, this.getHeight()));
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		JLabel title = new JLabel("Vehicles Log");
		logVehicle.setPreferredSize(new Dimension(250, 450));
		panel.add(title);
		panel.add(logVehicle);
		panel.add(new JSeparator());
		panel.add(new JLabel("Zoom "));
		
		//Button to increment zoom
		JButton zoomPlus = new JButton("+");
		zoomPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Double z = MapDrawer.getZoom();
				z += 0.05;
				if (z > 1.0) {
					z = 1.0;
				}
				MapDrawer.setZoom(z);
			}
		});
		panel.add(zoomPlus);
		
		//Button to decrement zoom
		JButton zoomMinus = new JButton("-");
		zoomMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Double z = MapDrawer.getZoom();
				z -= 0.05;
				if (z < 0.5) {
					z = 0.5;
				}
				MapDrawer.setZoom(z);
			}
		});
		panel.add(zoomMinus);
		
		this.getContentPane().add(panel, BorderLayout.EAST);
		this.setPreferredSize(new Dimension(1000, 700));
		this.setSize(1000, 600);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		 
		int w = this.getSize().width;
		int h = this.getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		 
		this.setLocation(x, y);
		this.setVisible(true);
		this.setTitle("Ca.R.S - Cars on the Road Simulator");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	/**
	 * Clear the vehicle list and rewrote it updated.
	 * All dead vehicles are put at the end of the list.
	 */
	public static void refreshLogPanel() {
		logVehicleList.clear();
		for (IModelVehicle tmp : Garage.getInstance().getVehicles()) {
			if (tmp.isRunning()) {
				logVehicleList.addElement(tmp.getName() + " is at " + tmp.getPosition());
			}
		}
		for (IModelVehicle tmp : Garage.getInstance().getVehicles()) {
			if (!tmp.isRunning()) {
				logVehicleList.addElement(tmp.getName() + " is dead.");
			}
		}
		
	}
	
	/**
	 * Passes the matrix at the mapDrawer object that draw the map in the frame.
	 * 
	 * @param mapGrid
	 *            the matrix used to draw the map
	 */
	public void drawMap(final String[][] mapGrid) {
		if (mapGrid != null) {
			map = new MapDrawer(mapGrid);
			map.setBorder(new LineBorder(new Color(0, 0, 0)));
			map.setPreferredSize(new Dimension(MapManager.getColumns() * ITile.TILE_EDGE, MapManager.getRows() * ITile.TILE_EDGE));
			JScrollPane scr = new JScrollPane(map, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
			this.getContentPane().add(scr);
			this.setVisible(true);
			if (!this.isLocationByPlatform() && !this.isVisible()) {
				this.setLocationByPlatform(true);
			}
			Thread t = new Thread(map);
			t.start();
		} else {
			Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Error while loading map.");
		}
	}
	
	/**
	 * Contains the method used to filter the selected file choose with JFileChooser.
	 */
	private class TxtFileFilter extends FileFilter {

	    public boolean accept(final File file) {
	      if (file.isDirectory()) {
	    	  return true;
	      }
	        String fname = file.getName().toLowerCase();
	        return fname.endsWith("txt");
	    }

	    public String getDescription() {
	      return "File di testo";
	    }
	  }
	
	/**
	 * Panel of spawn. Contains the possibility to choose the type of the vehicle, coordinates of spawn and eventually
	 * start a new spawn timer at fixed rate.
	 * In both cases vehicles can be automatically started.
	 * 
	 * @author Gioele Masini
	 */
	public class SpawnPanel extends JPanel {
		private final JComboBox<Object> selVehicle;
		private final JComboBox<Object> selPoint;
		private final JTextField fieldTimer;
		private final JCheckBox chkAutoStart;
		
		/**
		 * Construct a new panel of spawn.
		 */
		public SpawnPanel() {
				this.setLayout(new GridLayout(0, 2));
				
				//Vehicle type selection
				JLabel labelVehicles = new JLabel("Vehicle type: ");
				Object[] vehiclesTypes = Garage.getInstance().getAllVehiclesTypes().toArray();
				this.selVehicle = new JComboBox<>(vehiclesTypes);
				this.add(labelVehicles);
				this.add(this.selVehicle);
				
			    this.add(Box.createVerticalStrut(1)); // a spacer
			    this.add(Box.createVerticalStrut(1)); // a spacer
			    
				//Spawn point selection
				JLabel labelPoint = new JLabel("Spawn coordinates: ");
				List<Point> points = MapManager.getInstance().getSpawnPoints();
				Object[] spawnpoints;
				if (points.size() > 0) {
					spawnpoints = new Object[points.size() + 1];
					spawnpoints[0] = "Everywhere";
					System.arraycopy(points.toArray(), 0, spawnpoints, 1, points.size());
				} else {
					spawnpoints = new Object[]{};
				}
				this.selPoint = new JComboBox<>(spawnpoints);
				this.add(labelPoint);
				this.add(this.selPoint);
				
			    this.add(Box.createVerticalStrut(1)); // a spacer
			    this.add(Box.createVerticalStrut(1)); // a spacer
				
			    //Timer selection
			    JLabel labelTimer = new JLabel("Set spawn timer: ");
			    JPanel panelTimer = new JPanel(new GridLayout(1, 0));
			    this.fieldTimer = new JTextField();
			    panelTimer.add(this.fieldTimer);
			    panelTimer.add(new JLabel(" secs*"));
			    
			    this.add(labelTimer);
			    this.add(panelTimer);
			    
			    //Checkbox
			    this.add(Box.createVerticalStrut(1)); // a spacer
			    this.chkAutoStart = new JCheckBox();
			    this.chkAutoStart.setText("Start automatically");
			    this.chkAutoStart.setSelected(true);
			    this.add(this.chkAutoStart);
			    
			    //"Optional fields" label
			    JLabel endLabel = new JLabel("*Optional fields");
			    endLabel.setFont(endLabel.getFont().deriveFont(10f));
			    endLabel.setAlignmentY(BOTTOM_ALIGNMENT);
			    endLabel.setForeground(Color.DARK_GRAY);
			    this.add(endLabel);
		}
	}
}