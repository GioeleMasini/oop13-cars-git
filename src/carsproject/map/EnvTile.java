package carsproject.map;

import java.awt.Image;

import carsproject.general.Coordinates;

/**
 * First level specialization of {@link AbstractTile} class, represents the
 * environment except the route, like it could be a tree, a sidewalk or a gas
 * station.
 * <p>
 * Notes: as a first implementation we've decided to use only grass as
 * specialization of this class, calling the specialization {@link Base}.
 * <p>
 * 
 * @author Pruccoli Andrea
 * 
 */
public class EnvTile extends AbstractTile {

	/**
	 * Constructor of {@link EnvTile} class, calls the constructor of the
	 * super-class {@link AbstractTile} and assigns value 0 to the
	 * field {@link AbstractTile.possibleWays}.
	 * 
	 * @param sprite
	 *            the image used to portray the tile
	 * @param position
	 *            position expressed in {@link Coordinates}
	 */
	public EnvTile(final Image sprite, final Coordinates position) {
		super(sprite, position);
		this.possibleWays = new Direction[] { Direction.ENVIRONMENT };
	}

	/**
	 * Second level specialization of {@link AbstractTile} class, represents the
	 * grass.
	 * 
	 * @author Pruccoli Andrea
	 * 
	 */
	public static class Base extends EnvTile {

		/**
		 * Constructor of {@link Base} class, call the constructor of the
		 * super-class {@link EnvTile}.
		 * 
		 * @param sprite
		 *            the image used to portray the tile
		 * @param position
		 *            position expressed in {@link Coordinates}
		 */
		public Base(final Image sprite, final Coordinates position) {
			super(sprite, position);
			this.type = TileType.BASE;
		}
	}
}
