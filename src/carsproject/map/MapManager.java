package carsproject.map;

import java.awt.Image;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import carsproject.general.Amanuensis;
import carsproject.general.Coordinates;
import carsproject.map.Roads.TrafficLight;
import carsproject.view.ImageTool;
import carsproject.view.MapDrawer;

/**
 * Contains the object and methods in order to handle the thraffic lights' and
 * zebra crossings' change of state. <div>Moreover contains the methods needed
 * in order to read the map from the .txt file.</div>
 * 
 * @author Pruccoli Andrea, Zamagna Marco
 * 
 */
public final class MapManager {

	/** Represents the singleton of the {@link MapManager} class. */
	private static MapManager SINGLETON = null;
	
	private static final String CFG_FILE_APPENDICE = ".cfg";

	/** Retrieve the singleton from the {@link MapTiles} class. */
	private static MapTiles mapTiles = null;

	/** */
	private static int rows = 0;

	/** */
	private static int columns = 0;

	/** Thread for the traffic lights' change of state. */
	private Timer threadTL;

	/** Thread for the zebra crossings' change of state. */
	//private ThreadZC threadZC;

	/** Checks the current situation of the simulation. */
	private Boolean started = false;

	/** Helps the association of traffic lights' crosses */
	private List<Coordinates> controlTemp = new ArrayList<>();

	/**
	 * Constructor of the class, gets the singleton of {@link MapTiles} class
	 * (if already allocated), calls the {@link MapTiles.searchSpecialTiles}
	 * method in order to group the traffic lights' crosses and the zebra
	 * crossings, allocate and runs the threads related to these last ones if
	 * there are tiles of these type in the world.
	 * 
	 * <p>
	 * Notes:<div>called by {@link MapManager.getMapManager} if the singleton
	 * hasn't been allocated yet</div>
	 * <p>
	 */
	private MapManager() {
	}

	/**
	 * Returns the singleton of the class if it has been allocated yet,
	 * otherwise calls the constructor of the class which allocates the
	 * singleton and returns it.
	 * 
	 * @return the singleton of the class
	 */
	public static MapManager getInstance() {
		if (SINGLETON == null) {
			SINGLETON = new MapManager();
		}
		return SINGLETON;
	}
	
	
	/**
	 * Method to load a map from its absolute path.
	 * 
	 * @param mapPath Path of the map to be loaded
	 * @return matrix of strings with read characters
	 */
	public static String[][] loadMap(final String mapPath) {
		String[][] mapGrid = MapManager.readMapFile(mapPath);
		List<String> cfgFile = MapManager.readCfgFile(mapPath);
		
		MapManager.rows = mapGrid.length;
		MapManager.columns = 0;
		for (String[] s : mapGrid) {
			if (s.length > MapManager.columns) {
				MapManager.columns = s.length;
			}
		}

		MapTiles.createInstance(MapManager.getRows(), MapManager.getColumns());
		MapManager.createMap(mapGrid);
		
		/* Setting up MapTiles instance */
		mapTiles = MapTiles.getInstance();
		/*if (MapManager.mapTiles.getZCList().size() != 0) {
			MapManager.getInstance().threadZC = new ThreadZC();
		}*/

		MapManager.processCfgFile(cfgFile);
		
		MapManager.getInstance().searchSpecialTiles();
		if (MapManager.mapTiles.getSemaphoreList().size() != 0) {
			MapManager.getInstance().threadTL = new Timer("Traffic-light crosses' manager");
			MapManager.getInstance().startSimulation();
		}
		
        return mapGrid;
	}
	
	/**
	 * Reads the file in input and create the map matrix.
	 * 
	 * @param mapPath absolute path of map file
	 * @return string representation of the map
	 */
	private static String[][] readMapFile(final String mapPath) {
		BufferedReader buffReader1;
		BufferedReader buffReader2;
		
		//Setting buffered reader
		if (mapPath.startsWith("/")) {
			buffReader1 = new BufferedReader(new InputStreamReader(MapManager.class.getResourceAsStream(mapPath)));
			buffReader2 = new BufferedReader(new InputStreamReader(MapManager.class.getResourceAsStream(mapPath)));
		} else {
			File mapFile = new File(mapPath);
			try {
		    	buffReader1 = new BufferedReader(new FileReader(mapFile));
		    	buffReader2 = new BufferedReader(new FileReader(mapFile));
			} catch (FileNotFoundException e) {
				//File not found
				e.printStackTrace();
				return null;
			}
		}
		
		//Reading the map
		String[][] mapGrid = null;
		int rows = 0;
		int columns = 0;
		try {

			BufferedReader readFile = buffReader1;

			String stringRead = readFile.readLine();
			rows++;
			while (stringRead != null) {
				int tmp = stringRead.length();
				if (columns < tmp) {
					columns = tmp;
				}

				stringRead = readFile.readLine();
				rows++;
			}
			rows--;
			readFile.close();

			BufferedReader readFile2 = buffReader2;
			stringRead = readFile2.readLine();
			mapGrid = new String[rows][columns];
			int i = 0;
			while (stringRead != null) {
				int tmp = stringRead.length();

				for (int j = 0; j < columns; j++) {
					if (j < tmp) {
						mapGrid[i][j] = "" + stringRead.charAt(j);
					} else {
						mapGrid[i][j] = "";
					}
				}

				stringRead = readFile2.readLine();
				i++;
			}
			readFile2.close();

		} catch (Exception e) {
			System.out.println("Error on trying to read the file. " + e);
		}
		MapManager.rows = rows;
		MapManager.columns = columns;
		return mapGrid;
	}
	
	/**
	 * Function to read cfg file of a map.
	 * 
	 * @param mapPath absolute path of map file.
	 * @return the entire config file. Null if not found or if some IO errors.
	 */
	private static List<String> readCfgFile(final String mapPath) {
		//Calculating cfg file path from map file path
		String cfgPath = mapPath.substring(0, mapPath.length() - 4);
		cfgPath = cfgPath + MapManager.CFG_FILE_APPENDICE;
    	List<String> cfgFile = new LinkedList<String>();
    	
    	BufferedReader buffReader = null;
    	
    	if (cfgPath.startsWith("/")) {
    		//The file is in the jar
    		buffReader = new BufferedReader(new InputStreamReader(MapManager.class.getResourceAsStream(cfgPath)));
    	} else {
    		//The file has a standard path
    		try {
				buffReader = new BufferedReader(new FileReader(cfgPath));
			} catch (IOException e) {
				System.out.println("Configuration file not found.");
				//File not found, no configuration for the map
			}
    	}

    	//Starting to read the file, if founded
    	if (buffReader != null) {
			try {
				String line;
				while ((line = buffReader.readLine()) != null) {
					cfgFile.add(line);
				}
					buffReader.close();
			} catch (IOException e) {
				//Errors while reading the file
				e.printStackTrace();
			}
    	}
    	
		return cfgFile;
	}
	
	/**
	 * Process map configuration file and do necessary changes both in MapManager and in MapTiles.
	 * 
	 * @param cfgFile map configuration file
	 */
	private static void processCfgFile(final List<String> cfgFile) {
		//Check if the file is valid
		if (cfgFile != null) {
			//Processing line by line
			for (String line : cfgFile) {
				String[] lineData = MapManager.decodeCfgLine(line);
				if (lineData != null) {
					switch (lineData[0].toLowerCase()) {
					case "spawnpoint":	{
						if (lineData.length != 3) {
							break;	//Invalid number of parameters
						}
						
						try {
							Point spawnPoint = new Point(Integer.parseInt(lineData[1]),  Integer.parseInt(lineData[2]));
							MapManager.mapTiles.addSpawnCoordinates(pointToCoordinates(spawnPoint));
						} catch (Exception e) {
							Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Invalid line at map config file.");
						}
						break;
					}
					case "tlpriority": {
						if (lineData.length != 4) {
							break;	//Invalid number of parameters (need: name, two coordinates, priority value)
						}
						
						try {
							Point tlPoint = new Point(Integer.parseInt(lineData[1]),  Integer.parseInt(lineData[2]));
							ITile tl = MapTiles.getInstance().getTileAt(pointToCoordinates(tlPoint));
							if (tl.getType() == TileType.TRAFFIC_LIGHT) {
								((Roads.TrafficLight) tl).setPriority(Integer.parseInt(lineData[3]));
							} else {
								Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Invalid coordinates in map config file: " + tlPoint);
							}
						} catch (Exception e) {
							Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Invalid line at map config file.");
						}
						break;
					}
					default: 			{
						if (!(lineData[0] == "" && lineData.length == 1)) {
							Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Cannot read line from config map file");
						}
						break;
					}
					}
				}
			}
		}
	}
	
	/**
	 * Decode a line of text of map configuration file.
	 * Will return null if the line is invalid, an array of string with
	 * configuration line name and parameters in this scheme: {name, paramether1, paramether2, ...}.
	 * A comment line will made this return null. (A line is considered comment if starts with "//".
	 * 
	 * @param line line of text of configuration file.
	 * @return an array with configuration name and parameter in this line, null if given invalid line.
	 */
	private static String[] decodeCfgLine(String line) {
		//Check if there is a comment or an invalid line
		if (line == null || line.length() == 0 ||
					line.startsWith("//") || !line.endsWith(")") || !line.contains("(")) {
			return null;
		}
		
		//Reading value name and paramethers
		String valueName = line.substring(0, line.indexOf("("));
		String paramString = line.substring(line.indexOf("(") + 1, line.indexOf(")"));
		String[] parameters = paramString.split(",");
		
		//Creating an array with all values to return
		String[] returnValues = new String[parameters.length + 1];
		returnValues[0] = valueName;
		Integer i = 1;
		for (String s : parameters) {
			returnValues[i] = s;
			i++;
		}
		return returnValues;
	}
	
	/**
	 * Return all spawn points of the map with the same numbering used in configuration file.
	 * They indicate point at spawn through the number of row and column.
	 * 
	 * @return all spawn points of the map
	 */
	public List<Point> getSpawnPoints() {
		List<Coordinates> spawnCoords = MapManager.mapTiles.getSpawnCoordinates();
		List<Point> spawnPoints = new LinkedList<>();
		
		for (Coordinates c : spawnCoords) {
			spawnPoints.add(coordinatesToPoint(c));
		}
		
		return spawnPoints;
	}
	
	/**
	 * Conversion from coordinates to configuration file numbering.
	 * 
	 * @param coords coordinates to be converted
	 * @return equivalent point
	 */
	public static Point coordinatesToPoint(final Coordinates coords) {
		return new Point(coords.getY() / ITile.TILE_EDGE + 1, coords.getX() / ITile.TILE_EDGE + 1);
	}
	
	/**
	 * Conversion from coordinates to configuration file numbering.
	 * 
	 * @param point values to be converted
	 * @return equivalent coordinates
	 */
	public static Coordinates pointToCoordinates(final Point point) {
		return new Coordinates((point.b - 1) * ITile.TILE_EDGE, (point.a - 1) * ITile.TILE_EDGE);
	}
	

	/**
	 * Returns the rows of the map matrix.
	 * 
	 *@return 
	 *		int value of the rows
	 *
	 */
	public static int getRows() {
		return rows;
	}
	
	/**
	 * Returns the columns of the map matrix.
	 * 
	 *@return 
	 *		int value of the columns
	 *
	 */
	public static int getColumns() {
		return columns;
	}

	/**
	 * Creates the map by allocating the tiles depending on the character wrote
	 * in the .txt file. <div>Sets the max in the {@link Coordinates} class for
	 * the X and Y values.</div>
	 * 
	 * @param mapGrid
	 *            the String matrix taken from the .txt file
	 */
	public static void createMap(final String[][] mapGrid) {
		for (int i = 0; i < mapGrid.length; i++) {
			for (int j = 0; j < mapGrid[i].length; j++) {
				ITile tile = null;
				Coordinates coords = new Coordinates(j * ITile.TILE_EDGE, i * ITile.TILE_EDGE);
				switch (mapGrid[i][j]) {
				case "-":
					if (i < (mapGrid.length - 1)  && mapGrid[i + 1][j].compareTo("") != 0 && mapGrid[i + 1][j].compareTo(" ") != 0) {
						tile = new Roads.Road(MapDrawer.EW, coords, new Direction[] { Direction.W });
					} else if (i > 0  && mapGrid[i - 1][j].compareTo("") != 0 && mapGrid[i - 1][j].compareTo(" ") != 0) {
						tile = new Roads.Road(MapDrawer.EW, coords, new Direction[] { Direction.E });
					} else {
						tile = new EnvTile.Base(MapDrawer.EW, coords);
					}
					break;
				case "*":
					//Counting how many '*' are around my position
					Integer count = 0;
					//Check right
					if (j + 1 < mapGrid[i].length && mapGrid[i][j + 1].compareTo("*") == 0) {
						count++;
					}
					//Check left
					if (j - 1 >= 0 && mapGrid[i][j - 1].compareTo("*") == 0) {
						count++;
					}
					//Check top
					if (i + i < mapGrid.length && mapGrid[i + 1][j].compareTo("*") == 0) {
						count++;
					}
					//Check bot
					if (i - 1 >= 0 && mapGrid[i - 1][j].compareTo("*") == 0) {
						count++;
					}
					
					if (count == 0) {	//The tile must be a BOW
						List<Direction> possibleWays = new LinkedList<>();
						possibleWays.add(Direction.N);
						possibleWays.add(Direction.E);
						possibleWays.add(Direction.S);
						possibleWays.add(Direction.W);
						
						//Checking what are available ways
						if (j + 1 >= mapGrid[i].length || !MapManager.isRoad(mapGrid[i][j + 1]) || mapGrid[i][j + 1].compareTo("|") == 0) {
							//Nothing at EAST
							possibleWays.remove(Direction.W);
						}
						if (j - 1 < 0 || !MapManager.isRoad(mapGrid[i][j - 1]) || mapGrid[i][j - 1].compareTo("|") == 0) {
							//Nothing at WEST
							possibleWays.remove(Direction.E);
						}
						if (i + 1 >= mapGrid.length || !MapManager.isRoad(mapGrid[i + 1][j]) || mapGrid[i + 1][j].compareTo("-") == 0) {
							//Nothing at SOUTH
							possibleWays.remove(Direction.S);
						}
						if (i - 1 < 0 || !MapManager.isRoad(mapGrid[i - 1][j]) || mapGrid[i - 1][j].compareTo("-") == 0) {
							//Nothing at NORTH
							possibleWays.remove(Direction.N);
						}
						
						if (possibleWays.size() != 2) {
							//Array must be of size 1, checking what is the second direction deleted
							if (possibleWays.get(0) == Direction.N || possibleWays.get(0) == Direction.S) {
								//Horizontal direction deleted
								if (j - 1 < 0) {
									possibleWays.add(Direction.E);
								} else {
									possibleWays.add(Direction.W);
								}
							} else {
								//Vertical direction deleted
								if (i - 1 < 0) {
									possibleWays.add(Direction.N);
								} else {
									possibleWays.add(Direction.S);
								}
							}
						}
						
						//Now that directions are ok, calculating image
						Image img = null;
						Integer value = null;
						for (Integer index = 0; index < 2 && img == null; index++) {
							switch (possibleWays.get(index)) {
							case N:
								if (possibleWays.get((index + 1) % 2) == Direction.E) {
									img = MapDrawer.TURN_EN;
									value = 7;
								} else {
									img = MapDrawer.TURN_WN;
									value = 9;
								}
								break;
							case S:
								if (possibleWays.get((index + 1) % 2) == Direction.E) {
									img = MapDrawer.TURN_ES;
									value = 6;
								} else {
									img = MapDrawer.TURN_WS;
									value = 8;
								}
								break;
							default:
								break;
							}
						}
						
						//And finally setting the tile
						tile = new Roads.Bow(img, coords, possibleWays.toArray(new Direction[]{}), value);
					} else {
						//The tile must be a CROSS. Checking for possible ways.
						List<Direction> possibleWays = new LinkedList<>();
						possibleWays.add(Direction.N);
						possibleWays.add(Direction.E);
						possibleWays.add(Direction.S);
						possibleWays.add(Direction.W);
						
						//Checking the direction the vehicle is from
						if (j - 1 > 0 && i - 1 > 0 && MapManager.isNotCross(mapGrid[i][j - 1]) && MapManager.isNotCross(mapGrid[i - 1][j])) {
							//Vehicle from NORTH
							possibleWays.remove(Direction.N);
						} else if (j + 1 < mapGrid[i].length && i - 1 > 0 && MapManager.isNotCross(mapGrid[i][j + 1]) && MapManager.isNotCross(mapGrid[i - 1][j])) {
							//Vehicle from EAST
							possibleWays.remove(Direction.E);
						} else if (j + 1 < mapGrid[i].length && i + 1 < mapGrid.length && MapManager.isNotCross(mapGrid[i][j + 1]) && MapManager.isNotCross(mapGrid[i + 1][j])) {
							//Vehicle from SOUTH
							possibleWays.remove(Direction.S);
						} else if (j - 1 > 0 && i + 1 < mapGrid.length - 2 && MapManager.isNotCross(mapGrid[i][j - 1]) && MapManager.isNotCross(mapGrid[i + 1][j])) {
							//Vehicle from WEST
							possibleWays.remove(Direction.W);
						}
					
						//Checking ways not available
						if (i - 2 > 0 && !MapManager.isRoad(mapGrid[i - 1][j]) && !MapManager.isRoad(mapGrid[i - 2][j])) {
							//North not exists
							possibleWays.remove(Direction.N);
						}
						if (i + 2 >= mapGrid.length || (!MapManager.isRoad(mapGrid[i + 1][j]) && !MapManager.isRoad(mapGrid[i + 2][j]))) {
							//South not exists
							possibleWays.remove(Direction.S);
						}
						if (j + 2 >= mapGrid[i].length || (!MapManager.isRoad(mapGrid[i][j + 1]) && !MapManager.isRoad(mapGrid[i][j + 2]))) {
							//East not exists
							possibleWays.remove(Direction.E);
						}
						if (j - 2 > 0 && !MapManager.isRoad(mapGrid[i][j - 1]) && !MapManager.isRoad(mapGrid[i][j - 2])) {
							//West not exists
							possibleWays.remove(Direction.W);
						}
						
						tile = new Roads.Cross(MapDrawer.INCROCIO, coords, possibleWays.toArray(new Direction[]{}));
					}
					/*} else {
						if (mapGrid[i][j - 1].compareTo("-") == 0) {
							if (i < (mapGrid.length - 1) && mapGrid[i + 1][j].compareTo("|") == 0) {
								tile = new Roads.Bow(MapDrawer.TURN_ES, coords, new Direction[] {Direction.E, Direction.S }, 6);
							} else {
								tile = new Roads.Bow(MapDrawer.TURN_EN, coords, new Direction[] {Direction.E, Direction.N }, 7);
							}
						} else {
							tile = new EnvTile.Base(MapDrawer.INCROCIO, coords);
						}*/
					break;
				case "|":
					if (j < (mapGrid[i].length - 1) && mapGrid[i][j + 1].compareTo("") != 0 && mapGrid[i][j + 1].compareTo(" ") != 0) {
						tile = new Roads.Road(MapDrawer.NS, coords, new Direction[] { Direction.S });
					} else if (j > 0 && mapGrid[i][j - 1].compareTo("") != 0 && mapGrid[i][j - 1].compareTo(" ") != 0) {
						tile = new Roads.Road(MapDrawer.NS, coords, new Direction[] { Direction.N });
					} else {
						tile = new EnvTile.Base(MapDrawer.NS, coords);
					}
					break;
				case "S":
					if (j < (mapGrid[i].length - 2) && mapGrid[i][j + 1].compareTo("*") == 0 && mapGrid[i][j + 2].compareTo("*") == 0 && mapGrid[i - 1][j].compareTo("-") == 0) {
						tile = new Roads.Stop(MapDrawer.STOP_E, coords, new Direction[] { Direction.E });
					} else if (i < (mapGrid.length - 2) && mapGrid[i + 1][j].compareTo("*") == 0 && mapGrid[i + 2][j].compareTo("*") == 0 && mapGrid[i][j + 1].compareTo("|") == 0) {
						tile = new Roads.Stop(MapDrawer.STOP_S, coords, new Direction[] { Direction.S });
					} else if (j > 1 && mapGrid[i][j - 1].compareTo("*") == 0 && mapGrid[i][j - 2].compareTo("*") == 0 && mapGrid[i + 1][j].compareTo("-") == 0) {
						tile = new Roads.Stop(MapDrawer.STOP_W, coords, new Direction[] { Direction.W });
					} else if (i > 1 && mapGrid[i - 1][j].compareTo("*") == 0 && mapGrid[i - 2][j].compareTo("*") == 0 && mapGrid[i][j - 1].compareTo("|") == 0) {
						tile = new Roads.Stop(MapDrawer.STOP_N, coords, new Direction[] { Direction.N });
					} else {
						if (mapGrid[i][j - 1].compareTo("-") == 0) {
							Direction[] direction = MapTiles.getInstance().getTileAt(new Coordinates((j * ITile.TILE_EDGE) - ITile.TILE_EDGE, (i * ITile.TILE_EDGE) - ITile.TILE_EDGE)).getPossibleWays();
							tile = new Roads.Road(MapDrawer.EW, coords, direction);
						} else {
							Direction[] direction = MapTiles.getInstance().getTileAt(new Coordinates((j * ITile.TILE_EDGE) - ITile.TILE_EDGE, (i * ITile.TILE_EDGE) - ITile.TILE_EDGE)).getPossibleWays();
							tile = new Roads.Road(MapDrawer.NS, coords, direction);
						}
					}
					break;
				case "P":
					if (j < (mapGrid[i].length - 2) && mapGrid[i][j + 1].compareTo("*") == 0 && mapGrid[i][j + 2].compareTo("*") == 0 && mapGrid[i - 1][j].compareTo("-") == 0) {
						tile = new Roads.Priority(MapDrawer.PRIOR_E, coords, new Direction[] { Direction.E });
					} else if (i < (mapGrid.length - 2) && mapGrid[i + 1][j].compareTo("*") == 0 && mapGrid[i + 2][j].compareTo("*") == 0 && mapGrid[i][j + 1].compareTo("|") == 0) {
						tile = new Roads.Priority(MapDrawer.PRIOR_S, coords, new Direction[] { Direction.S });
					} else if (j > 1 && mapGrid[i][j - 1].compareTo("*") == 0 && mapGrid[i][j - 2].compareTo("*") == 0 && mapGrid[i + 1][j].compareTo("-") == 0) {
						tile = new Roads.Priority(MapDrawer.PRIOR_W, coords, new Direction[] { Direction.W });
					} else if (i > 1 && mapGrid[i - 1][j].compareTo("*") == 0 && mapGrid[i - 2][j].compareTo("*") == 0 && mapGrid[i][j - 1].compareTo("|") == 0) {
						tile = new Roads.Priority(MapDrawer.PRIOR_N, coords, new Direction[] { Direction.N });
					} else {
						if (mapGrid[i][j - 1].compareTo("-") == 0) {
							Direction[] direction = MapTiles.getInstance().getTileAt(new Coordinates((j * ITile.TILE_EDGE) - ITile.TILE_EDGE, (i * ITile.TILE_EDGE) - ITile.TILE_EDGE)).getPossibleWays();
							tile = new Roads.Road(MapDrawer.EW, coords, direction);
						} else {
							Direction[] direction = MapTiles.getInstance().getTileAt(new Coordinates((j * ITile.TILE_EDGE) - ITile.TILE_EDGE, (i * ITile.TILE_EDGE) - ITile.TILE_EDGE)).getPossibleWays();
							tile = new Roads.Road(MapDrawer.NS, coords, direction);
						}
					}
					break;
				case "T":
					try {
						Image[] tlSprites = new Image[]{MapDrawer.TL_GREEN, MapDrawer.TL_YELLOW, MapDrawer.TL_RED};
						if (j < (mapGrid[i].length - 2) && mapGrid[i][j + 1].compareTo("*") == 0 && mapGrid[i][j + 2].compareTo("*") == 0 && mapGrid[i - 1][j].compareTo("-") == 0) {
							//Rotating images at East
							Image[] temp = new Image[]{ImageTool.rotate(tlSprites[0], 90), ImageTool.rotate(tlSprites[1], 90), ImageTool.rotate(tlSprites[2], 90)};
							tile = new Roads.TrafficLight(temp, coords, new Direction[] { Direction.E });
						} else if (i < (mapGrid.length - 2) && mapGrid[i + 1][j].compareTo("*") == 0 && mapGrid[i + 2][j].compareTo("*") == 0 && mapGrid[i][j + 1].compareTo("|") == 0) {
							//Rotating images at South
							Image[] temp = new Image[]{ImageTool.rotate(tlSprites[0], 180), ImageTool.rotate(tlSprites[1], 180), ImageTool.rotate(tlSprites[2], 180)};
							tile = new Roads.TrafficLight(temp, coords, new Direction[] { Direction.S });
						} else if (j > 1 && mapGrid[i][j - 1].compareTo("*") == 0 && mapGrid[i][j - 2].compareTo("*") == 0 && mapGrid[i + 1][j].compareTo("-") == 0) {
							//Rotating images at West
							Image[] temp = new Image[]{ImageTool.rotate(tlSprites[0], 270), ImageTool.rotate(tlSprites[1], 270), ImageTool.rotate(tlSprites[2], 270)};
							tile = new Roads.TrafficLight(temp, coords, new Direction[] { Direction.W });
						} else if (i > 1 && mapGrid[i - 1][j].compareTo("*") == 0 && mapGrid[i - 2][j].compareTo("*") == 0 && mapGrid[i][j - 1].compareTo("|") == 0) {
							//Images already at North
							tile = new Roads.TrafficLight(tlSprites, coords, new Direction[] { Direction.N });
						} else {
							if (mapGrid[i][j - 1].compareTo("-") == 0) {
								Direction[] direction = MapTiles.getInstance().getTileAt(new Coordinates((j * ITile.TILE_EDGE) - ITile.TILE_EDGE, (i * ITile.TILE_EDGE) - ITile.TILE_EDGE)).getPossibleWays();
								tile = new Roads.Road(MapDrawer.EW, coords, direction);
							} else {
								Direction[] direction = MapTiles.getInstance().getTileAt(new Coordinates((j * ITile.TILE_EDGE) - ITile.TILE_EDGE, (i * ITile.TILE_EDGE) - ITile.TILE_EDGE)).getPossibleWays();
								tile = new Roads.Road(MapDrawer.NS, coords, direction);
							}
						}
					} catch (IOException ex) {
						tile = null;
						Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Error while retrieve traffic lights images.");
						ex.printStackTrace();
					}
					break;
				default:
					tile = new EnvTile.Base(MapDrawer.getGrassImage(), coords);
					break;
				}
				//if (tile.getType() != TileType.BASE) System.out.println("(" + i + "," + j + ") " + mapGrid[i][j] + " " + tile.getType());
				MapTiles.getInstance().addTile(tile);
			}
		}

		/*
		 * Sets the max values in class Coordinates
		 */
		Coordinates.setMaxX(mapGrid[0].length * ITile.TILE_EDGE);
		Coordinates.setMaxY(mapGrid.length * ITile.TILE_EDGE);
	}
	
	/**
	 * Return true if the string represent a road or a base tile, but NOT a cross.
	 * @param s String to be checked
	 * @return true if the string represent a road or a base tile, but NOT a cross.
	 */
	private static boolean isNotCross(final String s) {
		return (MapManager.isRoad(s) || s.equals(" ") || s.equals(""));
	}
	
	/**
	 * Return true if the string represent a road, but NOT a cross.
	 * @param s String to be checked
	 * @return true if the string represent a road, but NOT a cross.
	 */
	private static boolean isRoad(String s) {
		s = s.toUpperCase();
		return (s.equals("|") || s.equals("-") || s.equals("S") || s.equals("T") || s.equals("Z") || s.equals("P"));
	}

	/**
	 * Starts the simulation if it hasn't been started yet, otherwise it does
	 * anything, so it can't mess the current simulation.<div>Calls the
	 * {@link Timer.schedule} method on the thread charged of handling the
	 * traffic light, allocating a new {@link TimerTask} object and calling the
	 * routine every second.</div><div>Calls the {@link Thread.start} method on
	 * the thread charged of handling the zebra crossings.</div><div>Sets the
	 * {@link MapManager.started} field to true.</div>
	 * <p>
	 * Notes:<div>The thread's methods are called only if needed.</div>
	 * <p>
	 */
	public void startSimulation() {
		if (!this.started) {
			if (MapManager.mapTiles.getSemaphoreList().size() != 0) {
				this.threadTL.schedule(new TimerTask() {

					@Override
					public void run() {

						/*
						 * here is where we will call the method for the check
						 * of color change of each traffic-light cross
						 */
						this.check(MapManager.mapTiles.getSemaphoreList().iterator());
					}

					/*
					 * iters till reachs the end
					 */
					private void check(final Iterator<Semaphore> temp) {
						while (temp.hasNext()) {
							temp.next().checkForChanges();
						}
					}

				}, new Date(), 1000);
			}
		/*	if (MapManager.mapTiles.getZCList().size() != 0) {
				this.threadZC.start();
				try {
					this.threadZC.join();
				} catch (InterruptedException e) {
				}
			}*/
			this.started = true;
		}
	}

	/**
	 * Iterates the matrix mapping the map in search of tiles to be grouped such
	 * as {@link Roads.TrafficLight} and {@link Roads.Zebra}.
	 */
	public void searchSpecialTiles() {
		ITile[][] generalMap = MapTiles.getInstance().getMap();
		for (int i = 0; i < generalMap.length; i++) {
			for (int j = 0; j < generalMap[i].length; j++) {
				if (generalMap[i][j].getType() == TileType.TRAFFIC_LIGHT) {
					this.createAssociations(TileType.TRAFFIC_LIGHT, new Coordinates(j * ITile.TILE_EDGE, i * ITile.TILE_EDGE));
				} /*else if (this.generalMap[i][j].getType() == TileType.ZEBRA_CROSSING) {
					this.createAssociations(TileType.ZEBRA_CROSSING, new Coordinates(i * ITile.TILE_EDGE, j * ITile.TILE_EDGE));
				}*/
			}
		}
	}

	/**
	 * Calls the controlArea method in order to create the right associations
	 * between traffic lights of a same cross or nearby zebra crossings
	 * 
	 * <p>
	 * Notes: called by {@link searchSpecialTiles}
	 * <p>
	 * <p>
	 * 
	 * @param type
	 *            the type of the tile of which the association is needed to be
	 *            created
	 * @param pos
	 *            the position in {@link Coordinates} of the tile under
	 *            consideration
	 */
	private void createAssociations(TileType type, Coordinates pos) {
 		if (this.containsCoordinates(controlTemp, pos)) {
		} else if (type == TileType.TRAFFIC_LIGHT) {
			List<ITile> tempList = new ArrayList<>();
			tempList.addAll(this.controlArea(new ArrayList<ITile>(), pos, type, TileType.CROSS));
			if (tempList.size() != 0){
				/*
				 * updating trafficLights variable, now I've grouped traffic lights
				 * of the same cross
				 */
				List<Roads.TrafficLight> tlList = new ArrayList<>();
				for (ITile t : tempList) {
					if (t.getType() == TileType.TRAFFIC_LIGHT) {
						tlList.add((TrafficLight) t);
					} else {
						throw new IllegalStateException("Error on controlArea method. One or more tiles returned are not Traffic Lights.");
					}
				}
				MapTiles.getInstance().addSemaphore(new Semaphore(tlList));
			}
		}/* else if (type == TileType.ZEBRA_CROSSING) {
			List<ITile> tempList = new ArrayList<>();
			this.controlArea(tempList, pos, type, null);
			
			if (tempList.size() != 0){
				// updating zebraCrossing variable, now I've grouped zebra crossings
				this.zebraCrossing.add(tempList.toArray(new Roads.Zebra[tempList.size()]));
			}
		}*/
	}

	/**
	 * Controls the area around the tile at the given coordinates, if it's of
	 * the type needed adds it to the temporary list
	 * 
	 * <p>
	 * Notes: called by {@link createAssociations}
	 * 
	 * @param tempList
	 *            a temporary list which helps the association's creation
	 * @param position
	 *            the coordinates of the tile under consideration
	 * @param type
	 *            the type of the tile of interest
	 * @param support
	 *            a tile's type supporting the association's creation
	 */
	private List<ITile> controlArea(List<ITile> tempList, Coordinates position, TileType type, TileType support) {
		ITile tempTile = MapTiles.getInstance().getTileAt(position);
		if (tempTile == null || this.containsCoordinates(controlTemp, position)) {
			//No tile at that position or tile already checked
			return tempList;
		}

		//Adding the coordinates passed as input to the list of already controlled ones
		this.controlTemp.add(position);
		
		if (tempTile.getType() != type && tempTile.getType() != support){
			//Tile not important, can exit
			return tempList;
		}

		if (tempTile.getType() == type) {			//Checking if this is the tile i need
			tempList.add(tempTile);
		}
		
		//Recursive checking in all adjacent tiles
		// tile above
		Coordinates pos = new Coordinates(position.getX(), position.getY() - ITile.TILE_EDGE);
		tempList.addAll(this.controlArea(new ArrayList<ITile>(), pos, type, support));
		
		// tile at the left
		pos = new Coordinates(position.getX() - ITile.TILE_EDGE, position.getY());
		tempList.addAll(this.controlArea(new ArrayList<ITile>(), pos, type, support));

		// tile at the right
		pos = new Coordinates(position.getX() + ITile.TILE_EDGE, position.getY());
		tempList.addAll(this.controlArea(new ArrayList<ITile>(), pos, type, support));

		// tile below
		pos = new Coordinates(position.getX(), position.getY() + ITile.TILE_EDGE);
		tempList.addAll(this.controlArea(new ArrayList<ITile>(), pos, type, support));
		
		return tempList;
	}

	/**
	 * Checks the controlTemp in order to acknowledge if the Coordinates are
	 * memorized yet
	 * 
	 * @param container
	 *            the list supporting the associations of traffic lights'
	 *            crosses
	 * @param toCheck
	 *            the coordinates under consideration
	 * @return true if the coordinates are present yet, false otherwise
	 */
	private boolean containsCoordinates(List<Coordinates> container,
			Coordinates toCheck) {

		Iterator<Coordinates> iter = container.iterator();

		while (iter.hasNext()) {
			Coordinates now = iter.next();
			if (Coordinates.isLegal(now) && Coordinates.isLegal(toCheck)) {
				if (now.getX() == toCheck.getX() && now.getY() == toCheck.getY()) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Nested class created in order to handle, by a thread, the change of state
	 * of the {@link Roads.Zebra} tiles
	 * 
	 * @author Andrea
	 * 
	 */
//	private static class ThreadZC extends Thread {
//		
//		/** Constants used in order to handle the zebra crossing's change of state. */
//		private static final int ELAPSED_TIME = 11;
//		
//		/**
//		 * Run method of the thread, at pseudo-fixed times changes the state of
//		 * the zebra crossings which had been grouped scanning the mapping
//		 */
//		@Override
//		public void run() {
//			List<Roads.Zebra[]> list = MapTiles.getInstance().getZCList();
//			while (true) {
//				/*
//				 * set the field occupied to true
//				 */
//				for (int i = 0; i < list.size(); i += 2) {
//					Roads.Zebra[] temp = list.get(i);
//					this.applyChange(temp);
//				}
//				try {
//					Thread.sleep(1000 * (int) (Math.random() * 100) % ELAPSED_TIME);
//				} catch (InterruptedException e) {
//				}
//				/*
//				 * set the field occupied to false again
//				 */
//				for (int i = 0; i < list.size(); i += 2) {
//					Roads.Zebra[] temp = list.get(i);
//					this.applyChange(temp);
//				}
//				try {
//					Thread.sleep(1000 * (int) (Math.random() * 100) % 4);
//				} catch (InterruptedException e) {
//				}
//				/*
//				 * set the field of the others to true
//				 */
//				for (int j = 1; j < list.size(); j += 2) {
//					Roads.Zebra[] temp = list.get(j);
//					this.applyChange(temp);
//				}
//				try {
//					Thread.sleep(1000 * (int) (Math.random() * 100) % ELAPSED_TIME);
//				} catch (InterruptedException e) {
//				}
//				/*
//				 * set the field of the others to false again
//				 */
//				for (int j = 1; j < list.size(); j += 2) {
//					Roads.Zebra[] temp = list.get(j);
//					this.applyChange(temp);
//				}
//			}
//		}
//
//		/**
//		 * Changes the state of the zebra crossings' array passed as parameter
//		 * <p>
//		 * Notes: called by the run method in {@link MapManager.ThreadZC}
//		 * <p>
//		 * 
//		 * @param temp
//		 *            the array of zebra crossings' tiles which states have to
//		 *            be modified
//		 */
//		private void applyChange(final Roads.Zebra[] temp) {
//			for (int i = 0; i < temp.length; i++) {
//				temp[i].changeState();
//				System.out.println(temp[i]);
//			}
//		}
//	}
	
	/**
	 * Simple and general class that consists of two final integer values.
	 * 
	 * @author Gio
	 */
	public static class Point {
		/** First value of the couple. **/
		public final Integer a;
		/** Second value of the couple. **/
		public final Integer b;
		
		/**
		 * Unique setting of two final values of the Point's instance.
		 * 
		 * @param a first value
		 * @param b second value
		 */
		public Point(final Integer a, final Integer b) {
			this.a = a;
			this.b = b;
		}
		
		public String toString() {
			return "(" + a + ", " + b + ")";
		}
	}
}
