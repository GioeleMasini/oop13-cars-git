package carsproject.map;

/**
 * Type of the tiles which form the map, used in controls and assignments in
 * other classes.
 * 
 * @author Pruccoli Andrea
 * 
 */
public enum TileType {

	/**
	 * Environment tile, not integral part of the road.
	 */
	BASE,

	/**
	 * Simple straight or oblique road.
	 */
	ROAD,

	/**
	 * Tile in the center of a cross, allows the choice of multiple directions
	 * (if possible).
	 */
	CROSS,

	/**
	 * A bow on a way on the road.
	 */
	BOW,

	/**
	 * A traffic light before a cross.
	 */
	TRAFFIC_LIGHT,

	/**
	 * A stop before a cross, a car has to stop.
	 */
	STOP,

	/**
	 * A priority before a cross, a car has to stop only if other cars are
	 * coming.
	 */
	PRIORITY,

	/**
	 * A zebra crossing on the road, a car has to reduce its speed and, if
	 * needed, to stop.
	 */
	ZEBRA_CROSSING;
}
