package carsproject.map;

import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import carsproject.general.Coordinates;
import carsproject.map.MapManager.Point;
import carsproject.vehicles.AbstrModelVehicle;
import carsproject.vehicles.Garage;
import carsproject.vehicles.IModelVehicle;
import carsproject.vehicles.ThreadCar;

/**
 * Controller class of the view panel SpawnPanel.
 * Got methods to read from it and communicate with other controllers to spawn one vehicle
 * or to start a timer of spawn at fixed rate.
 * 
 * @author Gioele Masini
 */
public final class SpawnController {
	/** Singleton instance of the class. */
	private static final SpawnController SINGLETON = new SpawnController();
	/** List of active spawn timers. */
	private List<Timer> activeTimers = new LinkedList<Timer>();
	
	/**
	 * Unaccessible constructor, in order to follow singleton pattern.
	 */
	private SpawnController() {
	}
	
	/**
	 * Return the singleton instance of SpawnController.
	 * 
	 * @return the SINGLETON instance
	 */
	public static SpawnController getInstance() {
		return SpawnController.SINGLETON;
	}
	
	/**
	 * This method will spawn one vehicle or will start a timer of spawn reading fields
	 * of the panel stored in SpawnController.
	 */
	public void spawnVehicles(final String selectedPoint, final String selectedType, final Integer selectedTimer, final boolean autoStart) {
		String time = Long.toString(System.currentTimeMillis());
		String vehicleName = selectedType + time.substring(time.length() - 7);
		Coordinates selSpawnCoordinates = SpawnController.stringToSpawCoords(selectedPoint);
		
		if (selectedTimer == null || selectedTimer <= 0) {
			//If timer filed is not set or set with incomprehensible values: spawn of a new vehicle
			if (selSpawnCoordinates != null) {
				//If user selected a point, spawn vehicle here
				SpawnController.createVehicle(selectedType, vehicleName, selSpawnCoordinates, autoStart);
			} else {
				// If user selected "Everywhere", spawn a vehicle in every spawn point
				Integer n = 0;
				for (Point p : MapManager.getInstance().getSpawnPoints()) {
					Coordinates c = SpawnController.stringToSpawCoords(p.toString());
					SpawnController.createVehicle(selectedType, vehicleName + n, c, autoStart);
					n++;
				}
			}
		} else {
			//If timer field is set with a number: creation of a new timer with or without auto-start
			Integer seconds = selectedTimer;
			Integer delay = 0; 					//delay in milliseconds before first vehicle creation
			if (selSpawnCoordinates != null) {
				//If a point is selected, starting at timer for that point
				Timer timer = new Timer();
				timer.scheduleAtFixedRate(new SpawnTimerTask(selectedType, vehicleName, selSpawnCoordinates, autoStart), delay, seconds * 1000);
				this.activeTimers.add(timer);
			} else {
				//If "Everywhere" is selected, starting a timer for every point
				Integer n = 0;
				for (Point p : MapManager.getInstance().getSpawnPoints()) {
					delay = Math.round(Math.round((Math.random() * 1000 * (seconds))));	//Choosing a different delay for every timer, for a better simulation
					Coordinates c = SpawnController.stringToSpawCoords(p.toString());
					Timer timer = new Timer();
					timer.scheduleAtFixedRate(new SpawnTimerTask(selectedType, vehicleName + n, c, autoStart), delay, seconds * 1000);
					this.activeTimers.add(timer);
					n++;
				}
			}
		}
	}
	
	/**
	 * Method to return coordinates of spawn with a point string in input.
	 * Used for a better code organization and to reduce duplicated code.
	 * 
	 * @param point the string representation of a point
	 * @return coordinates of spawn for input point. Return null if the point is "Everywhere".
	 */
	private static Coordinates stringToSpawCoords(String point) {
		if (point.compareTo("Everywhere") != 0) {
			try {
				String[] temp = point.toString().substring(1, point.toString().length() - 1).trim().split(", ");
				//Calculating coordinates of spawn
				Point selSpawnPoint = new Point(Integer.parseInt(temp[0]), Integer.parseInt(temp[1]));
				Coordinates spawnCoords = MapManager.pointToCoordinates(selSpawnPoint);
				//Centering spawn on the tile
				spawnCoords.addY(ITile.TILE_EDGE / 2);
				spawnCoords.addX(ITile.TILE_EDGE / 2);
				return spawnCoords;
			} catch (Exception e) {
				//Errors on point reading
				return null;
			}
		}
		return null;
	}
	
	/**
	 * Create a new vehicle at given coordinates setting the right curvature and eventually starting it.
	 * 
	 * @param vehicleType type of the vehicle
	 * @param vehicleName name of the vehicle
	 * @param spawnCoords coordinates of the spawn
	 * @param autoStart if true the vehicle will be automatically started
	 * @return the new vehicle, null if it is not possible to spawn now a vehicle at those coordinates
	 */
	private static IModelVehicle createVehicle(final String vehicleType, final String vehicleName, final Coordinates spawnCoords, final boolean autoStart) {
		if (ThreadCar.isSpawnableAt(spawnCoords)) {
			IModelVehicle v = Garage.getInstance().createVehicle(vehicleType, vehicleName);
			if (v != null) {
				v.setPosition(spawnCoords);
				Direction vehicleDir = MapTiles.getInstance().getTileAt(spawnCoords).getPossibleWays()[0];
				v.setCurvatureAngleActual(Math.toRadians(AbstrModelVehicle.directionToDegrees(vehicleDir)));
				if (autoStart) {
					v.start();
				}
				return v;
			}
		}
		return null;
	}
	
	/**
	 * Stop all running timers of spawn.
	 */
	public void stopAllTimers() {
		for (Timer t : this.activeTimers) {
			t.cancel();
			t.purge();
		}
		this.activeTimers = new LinkedList<Timer>();
	}
	
	
	/**
	 * TimerTask with the purpose of create a new vehicle given its type, name and coordinates of spawn.
	 * If requested it will be automatically started. 
	 * 
	 * @author Gioele Masini
	 */
	private static class SpawnTimerTask extends TimerTask {		
		private String vehicleName;
		private String vehicleType;
		private Coordinates spawnCoordinates;
		private boolean autoStart;
		private Integer counter = 0;
		
		/**
		 * Construct a new spawn task.
		 * 
		 * @param vehicleType type of the vehicles
		 * @param vehicleName name of the vehicles (will be added a counter to differentiate them)
		 * @param spawnCoords coordinates of spawn
		 * @param autoStart if true the vehicle will be automatically started
		 */
		public SpawnTimerTask(final String vehicleType, final String vehicleName, final Coordinates spawnCoords, final boolean autoStart) {
			this.vehicleName = vehicleName;
			this.vehicleType = vehicleType;
			this.spawnCoordinates = spawnCoords;
			this.autoStart = autoStart;
		}
		
		@Override
		public void run() {
			IModelVehicle v = SpawnController.createVehicle(this.vehicleType, this.vehicleName + counter, this.spawnCoordinates, this.autoStart);
			if (this.autoStart && v != null) {
				Garage.getInstance().startVehicle(v.getName());
			}
			this.counter++;
		}
	}
}