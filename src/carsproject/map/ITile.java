package carsproject.map;

import java.awt.Image;

import carsproject.general.Coordinates;

/**
 * <p>
 * All Known Implementing Classes: {@link AbstractTile}
 * <p>
 * Allows the programmer to shape the tile (element which form the map) without
 * knowing what kind of tile it is.
 * 
 * @author Pruccoli Andrea
 * 
 */
public interface ITile {

	/** Represents the length of a tile in pixels. */
	public static final Integer TILE_EDGE = 40;

	/**
	 * Returns the run-time type of the tile under consideration.
	 * 
	 * @return the run-time type of the tile
	 * 
	 */
	public TileType getType();

	/**
	 * Returns the possible directions the car can take on the specified tile.
	 * 
	 * @return the possible directions the tiles allows to take
	 */
	public Direction[] getPossibleWays();

	/**
	 * Returns the sprite used to portray the tile.
	 * 
	 * @return the sprite of the tile
	 */
	public Image getSprite();

	/**
	 * Returns the position, on the map, of the tile under consideration.
	 * 
	 * @return the position of the tile in {@link Coordinates}
	 */
	public Coordinates getPosition();

	/**
	 * Sets the sprite used to portray the tile.
	 * 
	 * @param sprite
	 *            the image used to portray the tile
	 */
	public void setSprite(final Image sprite);

	/**
	 * Sets the position, on the map, of the tile under consideration.
	 * 
	 * @param coords
	 *            position expresses in {@link Coordinates}
	 */
	public void setPosition(final Coordinates coords);

	/**
	 * Sets the position, on the map, of the tile under consideration.
	 * 
	 * @param x
	 *            the row on the map
	 * @param y
	 *            the column on the map
	 */
	public void setPosition(final Integer x, final Integer y);

}
