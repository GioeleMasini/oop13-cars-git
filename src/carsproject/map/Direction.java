package carsproject.map;

/** Possible directions kept by a tile.
 * 
 * <p>Notes:
 * <div>- These are all the possible directions a tile can keep;</div>
 * <div>- {@link Roads.Bow} objects need, in their field {@link Roads.possibleWays},
 *  two of these directions;</div>
 * <div>- {@link Roads.Cross} objetcs need, in thei fiel {@link Roads.possibleWays},
 *  at least two of these directions.</div><p>
 * 
 * @author Pruccoli Andrea
 *
 */
public enum Direction {
	
	/** Default direction for the {@link EnvTile} tiles. */
	ENVIRONMENT,

	/** Represents the West direction. */
	W,
	
	/** Represents the East direction. */
	E,
	
	/** Represents the North direction. */
	N,
	
	/** Represents the South direction. */
	S,
	
	/** Represents the NorthWest direction. */
	NW,
	
	/** Represents the NorthEast direction. */
	NE,
	
	/** Represents the SouthEast direction. */
	SE,
	
	/** Represents the SouthWest direction. */
	SW;
	
	/**
     * Returns the opposite direction respect to that given.
     *
     * @param way direction input
     * @return opposite direction respect to input one, null if not supported.
     */
    public static Direction getContrary(final Direction way) {
            switch (way) {
            case N:         return S;
            case NE:        return SW;
            case E:         return W;
            case SE:        return NW;
            case S:         return N;
            case SW:        return NE;
            case W:         return E;
            case NW:        return SE;
            default:        return null;
            }
    }
   
    /**
     * Trasforms a Direction to a number.
     * This version handle only N, S, E and W.
     * This value is usable to calculate rotation degrees between two directions where
     * ((way2 - way1) * 90) give degrees between way1 and way2 that have to be
     * multiplied by -1 if one of two ways is S.
     *
     * @param way Direction to be converted
     * @return a number corresponding to Direction passed as input, null if value not supported.
     */
    public static Integer wayToNumber(final Direction way) {
            switch (way) {
            case N: return 0;
            case S: return 0;
            case E: return 1;
            case W: return -1;
            default: return null;
            }
    }
}
