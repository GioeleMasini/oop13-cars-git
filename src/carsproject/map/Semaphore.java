package carsproject.map;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import carsproject.map.Roads.TrafficLight;

/**
 * This class groups the traffic lights of a same cross.
 * 
 * @author Pruccoli Andrea
 * 
 */
public class Semaphore {

	/**
	 * Variable for the mapping of the time line of traffic lights' states'
	 * changes
	 */
	private Map<Integer, List<Roads.TrafficLight>> timeLine;

	/** Contains the time for a complete time span */
	private Integer timeSpan = -1;

	/** Used for a quick check of the keys in the timeLine map */
	private Integer[] toCheck;
	
	/** Used in toString */
	private List<TrafficLight> tlList;

	/** Date, in second, in which the semaphore has been started. Used to calculate change of colors. **/
	private Long startedTime = null;

	/**
	 * Constructor of {@link Semaphore} class, allocate the map, sets to 0
	 * timeSpan, calls the method for the disposal of traffic lights' order,
	 * calls the method to obtain the array toCheck.
	 * <p>
	 * Notes: <div>- called by {@link createAssociations()} in {@link MapTiles}.</div>
	 * <p>
	 * 
	 * @param list
	 *            list of the traffic lights in a same cross
	 */
	public Semaphore(final List<TrafficLight> list) {
		this.timeLine = new HashMap<>();
		this.timeSpan = 0;
		this.tlList = list;
		this.createCross(list);
		this.obtainChecker();
	}

	/**
	 * Defines the keys, which are the moment when the traffic lights in the
	 * related list has to change their states.
	 * 
	 * <p>
	 * Notes: <div>- called by the constructor of {@link Semaphore} class<div>
	 * <p>
	 * 
	 * @param list
	 *            list of the traffic lights in a same cross
	 */
	private void createCross(final List<TrafficLight> list) {
		//Checking if all traffic lights of the list got a priority value
		Integer n = 1;
		for (TrafficLight tl : list) {
			//If priorities not set, the method will set incremental ones and recall itself
			if (tl.getPriority() < 0) {
				tl.setPriority(n);
				n++;
			}
		}
		
		int turn = 0;
		List<TrafficLight> moment = new LinkedList<>();
		this.timeSpan = 0;

		/*
		 * Iterates until the are no more traffic lights in the list
		 */
		for (Integer listed = 0; listed != list.size(); turn++) {
			boolean founded = false;
			Iterator<TrafficLight> it = list.iterator();
			moment = new LinkedList<>();
			while (it.hasNext()) {
				TrafficLight tempo = (TrafficLight) it.next();
				if (tempo.getPriority() - 1 == turn) {
					moment.add(tempo);
					listed++;
					founded = true;
				}
			}
			
			if (founded) {
				// Add the right sequence
				this.timeLine.put(timeSpan, moment);
				this.timeSpan += moment.get(0).getGreenTime();
	
				this.timeLine.put(timeSpan, moment);
				this.timeSpan += TrafficLight.YELLOW_TIME;
	
				this.timeLine.put(timeSpan, moment);
				this.timeSpan += TrafficLight.GAP_TIME;
			}
		}
	}

	/**
	 * Gets the set of keys from the map, puts it in the array and sorts the
	 * array
	 */
	private void obtainChecker() {
		this.toCheck = new Integer[this.timeLine.keySet().size()];
		this.timeLine.keySet().toArray(toCheck);
		Arrays.sort(this.toCheck);
	}

	/**
	 * Changes the traffic lights' states by calling, on the traffic light under
	 * consideration, the {@link setColour()} method, incrementing their actual color
	 * 
	 * @param listToChange
	 *            list of tiles that need to change their color
	 */
	private void changeColour(final List<Roads.TrafficLight> listToChange) {
		if (listToChange == null) {
			return;
		}
		
		for (Roads.TrafficLight tl : listToChange) {
			tl.setColour((tl.getColour() + 1) % 3);
		}
	}

	/**
	 * Compares the elapsed time with the time interval and, as a
	 * consequence, change the states of the traffic lights in the cross
	 * 
	 * <p>
	 * Notes: <div>- called by thread's routine in {@link MapManager}.</div>
	 * <p>
	 */
	public void checkForChanges() {
		if (this.startedTime == null) {
			this.startedTime = System.currentTimeMillis() / 1000;
		}
		Long actualTime = System.currentTimeMillis() / 1000 - this.startedTime;
		Long moment = (actualTime % this.timeSpan);
		List<Roads.TrafficLight> tlToBeChanged = this.timeLine.get(moment.intValue());
		
		if (tlToBeChanged != null) {
			this.changeColour(tlToBeChanged);
		}
	}
	
	@Override
	public String toString() {
		return this.tlList.toString();
	}
}
