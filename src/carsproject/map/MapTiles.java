package carsproject.map;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import carsproject.general.Amanuensis;
import carsproject.general.Coordinates;

/**
 * Contains the objects needed in order to map the world under consideration, an
 * area of road traffic. <div>To this class are empowered the duties of
 * allocating the objects mentioned before, adding each tiles to the specified
 * position, and depending of which tiles are present, grouping the ones which
 * need to be handled by the {@link MapManager}.<div> <div>The class has been
 * organized as a singleton, because there will be only one generic situation
 * that will be handled at time.<div>
 * 
 * @author Masini Gioele, Pruccoli Andrea
 * 
 */
public class MapTiles {

	/**
	 * Represents the proportion between pixels and meters. In here are
	 * considered 40x40 pixels as 2.5x2.5 meters
	 */
	public static final Integer PIXEL_ON_METER = 16;

	/** Contains the reference to the tiles which form the map */
	private ITile[][] generalMap;

	/** List of traffic lights' crosses in the map */
	private List<Semaphore> trafficLights;

	/** List of the zebra crossings in the map */
	//private List<Roads.Zebra[]> zebraCrossing;
	
	/** List of possible spawn points */
	private List<Coordinates> spawnCoordinates = new LinkedList<Coordinates>();

	/** Singleton of the class */
	private static MapTiles mapTiles = null;

	/**
	 * Constructor of the {@link MapTiles} class, allocates the map depending on
	 * the size of the map given and the variables which are needed in order to
	 * manage the traffic lights' crosses and the zebra crossings
	 * 
	 * <p>
	 * Notes: called by {@link createInstance}
	 * <p>
	 * 
	 * @param mapHeight
	 *            the height of the map
	 * @param mapWidth
	 *            the width of the map
	 */
	private MapTiles(Integer mapHeight, Integer mapWidth) {
		this.generalMap = new ITile[mapHeight][mapWidth];
		this.trafficLights = new ArrayList<>();
		//this.zebraCrossing = new ArrayList<>();
	}

	/**
	 * Creates the instance of the class, calls the constructor of the class in
	 * order to allocate the singleton.
	 * 
	 * @param mapHeight
	 *            the height of the map
	 * @param mapWidth
	 *            the width of the map
	 * @throws IllegalStateException
	 *             if the singleton has been already allocated
	 */
	public static void createInstance(final Integer mapHeight, final Integer mapWidth) throws IllegalStateException {
		if (!MapTiles.isInstantiated()) {
			MapTiles.mapTiles = new MapTiles(mapHeight, mapWidth);
		} else {
			/*
			 * Instance already created
			 */
			Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Singleton of the class MapTiles already allocated");
			throw new IllegalStateException();
		}
	}

	/**
	 * Returns the singleton of the class.
	 * 
	 * @return the singleton of the class
	 * @throws IllegalStateException
	 *             if the singleton hasn't been allocated yet
	 */
	public static MapTiles getInstance() throws IllegalStateException {
		if (MapTiles.isInstantiated()) {
			return mapTiles;
		} else {
			/*
			 * Instance non created yet
			 */
			Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Singleton of the class MapTiles not allocated yet");
			throw new IllegalStateException();
		}
	}

	/**
	 * Checks if the singleton of the class has been allocated.
	 * 
	 * @return true if the singleton of the class has been already allocated,
	 *         false otherwise
	 */
	public static boolean isInstantiated() {
		return (mapTiles != null);
	}

	/**
	 * Converts pixels to meters.
	 * 
	 * @param pixel
	 *            the pixel to be transformed
	 * @return the related value in meters
	 */
	public static double pixelToMeters(final Integer pixel) {
		return pixel * (1 / MapTiles.PIXEL_ON_METER);
	}

	/**
	 * Converts meters to pixels.
	 * 
	 * @param meters
	 *            the meters to be transformed
	 * @return the related value in pixels
	 */
	public static Integer metersToPixel(final float meters) {
		return Math.round(meters * MapTiles.PIXEL_ON_METER);
	}

	/**
	 * Returns the list of the associated zebra crossings.
	 * 
	 * @return the list of the associated zebra crossings
	 */
	/*public List<Roads.Zebra[]> getZCList() {
		return this.zebraCrossing;
	}*/

	/**
	 * Returns the iterator of the associated traffic lights' crosses.
	 * 
	 * @return the iterator of the associated traffic lights' crosses
	 */
	public List<Semaphore> getSemaphoreList() {
		return this.trafficLights;
	}

	/**
	 * Returns the tile at the specified position.
	 * 
	 * @param pos
	 *            the position of the tile to obtain
	 * @return the tile at the specified position as an {@link ITile}
	 */
	public ITile getTileAt(final Coordinates pos) {
		try {
			return this.generalMap[pos.getY() / ITile.TILE_EDGE][pos.getX() / ITile.TILE_EDGE];
		} catch (Exception ex) {
			//In case of ArrayOutOfBoundsException and IllegalArgumentException of check methods of Coordinates
			//ex.printStackTrace();
			return null;
		}
	}

	/**
	 * Adds the tile passed as parameter at the position specified in the
	 * {@link ITile} object.
	 * 
	 * @param tile
	 *            the tile to be inserted
	 */
	public void addTile(final ITile tile) {
		this.generalMap[tile.getPosition().getY() / ITile.TILE_EDGE][tile.getPosition().getX() / ITile.TILE_EDGE] = tile;
	}
	
	/**
	 * Add a spawn point with coordinates to the map.
	 * 
	 * @param spawnPoint coordinates of spawn point
	 */
	public void addSpawnCoordinates(Coordinates spawnCoords) {
		if (spawnCoords != null) {
			if (this.getTileAt(spawnCoords).getType().equals(TileType.ROAD)) {
				this.spawnCoordinates.add(spawnCoords);
			}
		}
	}
	
	/**
	 * Return all spawn points of the map.
	 * 
	 * @return a list of coordinates of spawn points
	 */
	public List<Coordinates> getSpawnCoordinates() {
		return this.spawnCoordinates;
	}
	
	/**
	 * Return loaded map.
	 * 
	 * @return map used by program in this moment.
	 */
	public ITile[][] getMap() {
		return this.generalMap;
	}
	
	/**
	 * Add a new semaphore which will be managed by ThreadTL.
	 * 
	 * @param s the new semaphore to be added
	 */
	public void addSemaphore(Semaphore s) {
		this.trafficLights.add(s);
	}
}
