package carsproject.map;

import java.awt.Image;

import carsproject.general.Coordinates;

/**
 * Gives a first implementation of the interface ITile. It's recommended to
 * extend this class instead of implementing every time the ITile interface,
 * because the class allows the programmer to not implement the same methods
 * more than one time.
 * <p>
 * The only abstract methods are {@link getWay()} and {@link getPossibleWays()}.
 * <p>
 * 
 * @author Pruccoli Andrea
 * 
 */
public abstract class AbstractTile implements ITile {

	/** Image portraying the tile. */
	protected Image sprite;
	
	/** Position in the map of the tile. */
	protected Coordinates position;
	
	/** Type of the tile, useful for quick checks. */
	protected TileType type;
	
	/** Possible directions kept by a tile. */
	protected Direction[] possibleWays;

	/** Constructor of {@link AbstractTile} class, assigns the passed parameters
	 * to the respective variables.
	 * 
	 * @param sprite
	 *            the image used to portray the tile
	 * @param position
	 *            position expressed in {@link Coordinates}
	 */
	public AbstractTile(final Image sprite, final Coordinates position) {
		this.setSprite(sprite);
		this.setPosition(position);
	}

	@Override
	public Direction[] getPossibleWays() {
		return this.possibleWays;
	}

	@Override
	public TileType getType() {
		return this.type;
	}

	@Override
	public Image getSprite() {
		return this.sprite;
	}

	@Override
	public void setSprite(final Image sprite) {
		this.sprite = sprite;
	}

	@Override
	public Coordinates getPosition() {
		return this.position;
	}

	@Override
	public void setPosition(final Coordinates coords) {
		this.position = coords;
	}

	@Override
	public void setPosition(final Integer x, final Integer y) {
		this.position.setX(x);
		this.position.setY(y);
	}
	
	@Override
	public String toString() {
		return "Tile at " + this.position.toString() + " is a " + this.type;
	}

}
