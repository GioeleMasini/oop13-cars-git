package carsproject.map;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import carsproject.general.Amanuensis;
import carsproject.general.Coordinates;
import carsproject.vehicles.ModelCar;

/**
 * First level specialization of {@link AbstractTile} class, represents the
 * collection of road types, like it could be a simple straight road, or a bow,
 * or a traffic light etc...
 * 
 * @author Pruccoli Andrea
 * 
 */
public class Roads extends AbstractTile {	
	/** Boolean to know if the tile is occupied or not. */
	private boolean occupied;
	/** Name of the vehicle that occupied the tile. */
	private String occupiedName;


	/**
	 * Constructor of {@link Roads} class, call the constructor of the
	 * super-class {@link AbstractTile} and assigns the actual direction kept by
	 * the road.
	 * 
	 * @param sprite
	 *            the image used to portray the tile
	 * @param position
	 *            position expressed in {@link Coordinates}
	 * @param way
	 *            the actual direction kept by the tile
	 */
	public Roads(final Image sprite, final Coordinates position, final Direction[] way) {
		super(sprite, position);
		this.possibleWays = way;
		this.occupied = false;
	}

	/**
	 * Iterates the array in order to check the fidelity of the values of
	 * directions passed.
	 * 
	 * @param way
	 *            the array of directions kept by the tile
	 * @throws IllegalArgumentException
	 *             if the array of directions passed as arguments has an invalid
	 *             value
	 */
	protected void checkWays(final Direction[] way) throws IllegalArgumentException {

		/** Used to check if the directions' array passed is a valid one */
		Boolean local = false;

		for (Direction i : way) {
			for (Direction dir : Direction.values()) {
				if (i == dir) {
					local = true;
				}
			}
		}

		if (!local) {
			Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "The direction passed as argument is invalid");
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Check if the tile is occupied or not.
	 * 
	 * @return true if is occupied, false if not.
	 */
	public boolean isOccupied() {
		return this.occupied;
	}
	
	/**
	 * Return the name of the vehicle that occupied this tile.
	 * 
	 * @return name of the vehicle that occupied this tile
	 */
	public String getOccupyingVehicle() {
		if (this.isOccupied()) {
			return this.occupiedName;
		}
		return null;
	}
	
	/**
	 * Set the tile as occupied.
	 * 
	 * @param vehicleName name of the vehicle that will occupy that tile
	 * @throws IllegalStateException if the tile is already occupied.
	 */
	public synchronized void setOccupied(String vehicleName) throws IllegalStateException {
		if (this.occupied) {
			throw new IllegalStateException("Cross already occupied.");
		}
		this.occupied = true;
		this.occupiedName = vehicleName;
	}
	
	/**
	 * Set the tile as free (not occupied).
	 * 
	 * @param vehicleName name of the vehicle that will occupy that tile
	 */
	public void setFree(String vehicleName) {
		if (this.isOccupied() && this.occupiedName.compareTo(vehicleName) == 0) {
			this.occupied = false;
			this.occupiedName = null;
		}
	}

	/**
	 * Second level specialization of {@link AbstractTile} class, represents a
	 * simple straight road which keeps horizontal or diagonal directions.
	 * 
	 * @author Pruccoli Andrea
	 * 
	 */
	public static class Road extends Roads {

		/**
		 * Constructor of {@link Road} class, calls the constructor af the
		 * super-class {@link Roads} and assigns the type of the tile to
		 * {@link ROAD} of the {@link TileType} enum.
		 * 
		 * @param sprite
		 *            the image used to portray the tile
		 * @param position
		 *            position expressed in {@link Coordinates}
		 * @param way
		 *            the actual direction kept by the tile
		 */
		public Road(final Image sprite, final Coordinates position, final Direction[] way) {
			super(sprite, position, way);
			super.checkWays(this.possibleWays);
			this.type = TileType.ROAD;
		}
	}

	/**
	 * Second level specialization of {@link AbstractTile} class, represents a
	 * zebra crossing which could be present on the street. Can keep all the
	 * known directions, except for the ones reserved to {@link Roads.Bow} tiles
	 * 
	 * @author Pruccoli Andrea
	 * 
	 */
	public static class Zebra extends Roads {
		
		/**
		 * Constructor of {@link Zebra} class, calls the constructor af the
		 * super-class {@link Roads} and assigns the type of the tile to
		 * {@link ZEBRA_CROSSING} of the {@link TileType} enum.
		 * 
		 * @param sprite
		 *            the image used to portray the tile
		 * @param position
		 *            position expressed in {@link Coordinates}
		 * @param way
		 *            the actual direction kept by the tile
		 */
		public Zebra(final Image sprite, final Coordinates position, final Direction[] way) {
			super(sprite, position, way);
			super.checkWays(this.possibleWays);
			this.type = TileType.ZEBRA_CROSSING;
		}
		
		/**
		 * Set this zebra as occupied by a pedestrian.
		 */
		@SuppressWarnings("unused")
		private void setOccupiedByPedestrian() {
			super.setOccupied("Pedestrian" + this.position.toString());
		}
		
		/**
		 * Set this zebra as free from a pedestrian.
		 */
		public void setFreeFromPedestrian() {
			super.setFree("Pedestrian" + this.position.toString());
		}
		
		/**
		 * Returns the string indicating the current state of the zebra crossing.
		 * 
		 * @return the string indicating the current state of the zebra crossing
		 */
		public String toString() {
			if (this.isOccupied()) {
				return this.position.toString() + " Zebra crossing is occupied";
			}
			return this.position.toString() + " Zebra crossing is free";
		}
	}

	/**
	 * Second level specialization of {@link AbstractTile} class, represents a
	 * bow which only keeps East-North, East-South, West-North, West-South
	 * directions.
	 * 
	 * <p>Notes: as a first implementation has been decided to directly use the
	 * name of the sprites present in the project folder, so considering the fact
	 * the images have been saved by a progressive number, the control on the center
	 * of the bow uses integer variables.
	 * <p>
	 * 
	 * @author Pruccoli Andrea
	 * 
	 */
	public static class Bow extends Roads {

		/**
		 * Center of the bow needed to the {@link ModelCar} in order to turn
		 * its way
		 */
		private Coordinates centerBow;

		/**
		 * Constructor of the {@link Bow} class, calls the constructor of the
		 * super-class {@link Roads}, assigns the type of the tile to
		 * {@link BOW} of the {@link TileType} enum, the grade of the bow and
		 * the center of the bow expressed in {@link Coordinates}.
		 * 
		 * @param sprite
		 *            the image used to portray the tile
		 * @param position
		 *            position expressed in {@link Coordinates}
		 * @param way
		 *            the actual direction kept by the tile
		 * @param source
		 * 			  the name of the sprite used
		 */
		public Bow(final Image sprite, final Coordinates position, final Direction[] way, final int source) {
			super(sprite, position, way);
			this.checkWays(this.possibleWays);
			this.setCenterBow(source);
			this.type = TileType.BOW;
		}
		
		/**
		 * Checks if the length of the array is equals to 2, because all the
		 * bows are meant to have 2 directions, the one that comes and the
		 * one that goes.
		 * <div>After this check calls the same method of the superclass.</div>
		 * 
		 * @param way the way to be checked
		 */
		@Override
		protected void checkWays(final Direction[] way) {
			if (this.possibleWays.length != 2) {
				Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG,
						"A Bow tile is meant to have an array of two elements of Direction class!");
				throw new IllegalArgumentException();
			}
			super.checkWays(way);
		}

		/**
		 * Returns the center of the bow.
		 * 
		 * @return the center of the bow expressed in {@link Coordinates}
		 */
		public Coordinates getCenterBow() {
			return this.centerBow;
		}

		/**
		 * Sets the center of the bow depending on the direction kept by the bow
		 * 
		 * <p>
		 * Notes: <div>If the direction is South-West or East-North the center
		 * of the bow is the same as the normal coordinates of the tile.</div>
		 * 
		 * <div>If the direction is South-East or West-North the center of the bow
		 * is the position of the tile with the Y coordinate increased by 40.</div>
		 * 
		 * <div>If the direction is East-South or North-West the center of the bow
		 * is the position of the tile with the X coordinate increased by 40.</div>
		 * 
		 * <div>Finally, if the direction is West-South or North-East the center of the
		 * bow is the position of the tile with the X and Y coordinates increased by 40.</div>
		 * <p>
		 * 
		 * @param source
		 * 			  the name of the sprite used
		 */
		public void setCenterBow(final int source) {
			switch(source) {
			case 6: {
				this.centerBow = new Coordinates(this.position.getX(), this.position.getY() + ITile.TILE_EDGE);
				break;
			}
			case 7: {
				this.centerBow = this.position;
				break;
			}
			case 8: {
				this.centerBow = new Coordinates(this.position.getX() + ITile.TILE_EDGE, this.position.getY() + ITile.TILE_EDGE);
				break;
			}
			case 9: {
				this.centerBow = new Coordinates(this.position.getX() + ITile.TILE_EDGE, this.position.getY());
				break;
			}
			default: {
				this.centerBow = null;
				break;
			}
			}
		}

	}

	/**
	 * Second level specialization of {@link AbstractTile} class, represents a
	 * tile in the cross.
	 * 
	 * @author Pruccoli Andrea
	 * 
	 */
	public static class Cross extends Roads {
		/**
		 * Constructor of {@link Cross} class, calls the constructor af the
		 * super-class {@link Roads} and assigns the type of the tile to
		 * {@link CROSS} of the {@link TileType} enum.
		 * 
		 * @param sprite
		 *            the image used to portray the tile
		 * @param position
		 *            position expressed in {@link Coordinates}
		 * @param way
		 *            array of the possible ways expressed in Boolean[]
		 */
		public Cross(final Image sprite, final Coordinates position, final Direction[] way) {
			super(sprite, position, way);
			this.checkWays(this.possibleWays);
			this.type = TileType.CROSS;
		}
		
		/**
		 * Checks if the length of the array is higher than 1, because the
		 * crosses are meant to have more than 1 direction.
		 * <div>After this check calls the same method of the superclass.</div>
		 * 
		 * @param way the way to be checked
		 */
		@Override
		protected void checkWays(final Direction[] way) {
			if (this.possibleWays.length < 2) {
				Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG,
						"A Bow tile is meant to have an array of two elements of Direction class!");
				throw new IllegalArgumentException();
			}
			super.checkWays(way);
		}
	}

	/**
	 * Second level specialization of {@link AbstractTile} class, represents a
	 * stop.
	 * 
	 * @author Pruccoli Andrea
	 * 
	 */
	public static class Stop extends Roads {

		/**
		 * Constructor of {@link Stop} class, calls the constructor af the
		 * super-class {@link Roads} and assigns the type of the tile to
		 * {@link STOP} of the {@link TileType} enum.
		 * 
		 * @param sprite
		 *            the image used to portray the tile
		 * @param position
		 *            position expressed in {@link Coordinates}
		 * @param way
		 *            array of the possible ways expressed in Boolean[]
		 */
		public Stop(final Image sprite, final Coordinates position, final Direction[] way) {
			super(sprite, position, way);
			super.checkWays(this.possibleWays);
			this.type = TileType.STOP;
		}

	}

	/**
	 * Second level specialization of {@link AbstractTile} class, represents a
	 * priority.
	 * 
	 * @author Pruccoli Andrea
	 * 
	 */
	public static class Priority extends Roads {

		/**
		 * Constructor of {@link Priority} class, calls the constructor af the
		 * super-class {@link Roads} and assigns the type of the tile to
		 * {@link Priority} of the {@link TileType} enum.
		 * 
		 * @param sprite
		 *            the image used to portray the tile
		 * @param position
		 *            position expressed in {@link Coordinates}
		 * @param way
		 *            array of the possible ways expressed in Boolean[]
		 */
		public Priority(final Image sprite, final Coordinates position, final Direction[] way) {
			super(sprite, position, way);
			super.checkWays(this.possibleWays);
			this.type = TileType.PRIORITY;
		}

	}

	/**
	 * Second level specialization of {@link AbstractTile} class, represents a
	 * traffic light.
	 * 
	 * @author Pruccoli Andrea
	 * 
	 */
	public static class TrafficLight extends Roads {

		/** Default images used to portray the traffic light in its different states. */
		private static final String[] DEFAULT_SPRITES = new String[]{"res/images/TlGreen.png", "res/images/TlYellow.png", "res/images/TlRed.png"};
		
		/** Constant used as default value */
		private static final int DEFAULT_GREEN = 8;
		
		/** Constant used as max value which can be assigned to greenTime. */
		private static final int MAX_TIME = 20;
		
		/**
		 * Constant used in order to determine the time line in
		 * {@link Semaphore}.
		 */
		public static final int GAP_TIME = 2;

		/**
		 * Constant used in order to determine the time line in
		 * {@link Semaphore}.
		 */
		public static final int YELLOW_TIME = 2;
		
		/** Constant used for the state of the traffic light's colour. */
		public static final int GREEN_INDEX = 0;
		
		/** Constant used for the state of the traffic light's colour. */
		public static final int YELLOW_INDEX = 1;
		
		/** Constant used for the state of the traffic light's colour. */
		public static final int RED_INDEX = 2;
		
		/** Contains the sprites for the three states colour*/
		private Image[] sprites;
		
		/** Define the current state of the traffic light */
		private int colour;
		
		/** Define the time that's given to this traffic light */
		private Integer greenTime = DEFAULT_GREEN;
		
		/**
		 * Define the turn of this traffic light inside the cross in which is
		 * involved
		 */
		private int priority = -1;

		/**
		 * Constructor of {@link TrafficLight} class, calls the constructor af
		 * the super-class {@link Roads}, assigns the default state 2 (RED) and
		 * the type of the tile to {@link TRAFFIC_LIGHT} of the {@link TileType}
		 * enum.
		 * 
		 * @param sprites
		 *            the images used to portray the tile in its different states [Green,Yellow,Red]
		 * @param position
		 *            position expressed in {@link Coordinates}
		 * @param way
		 *            array of the possible ways expressed in Boolean[]
		 * @throws IOException
		 * 			  if the application couldn't retrieve images
		 */
		public TrafficLight(final Image[] sprites, final Coordinates position, final Direction[] way) throws IOException {
			super(sprites[RED_INDEX], position, way);
			super.checkWays(this.possibleWays);
			this.colour = RED_INDEX;
			this.type = TileType.TRAFFIC_LIGHT;
			try {
				this.setSprites(sprites);
			} catch (Exception e) {
				Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Couldn't retrieve images for the traffic light");
				throw new IOException("Problems while setting traffic light sprites.");
			}
		}
		
		/**
		 * Sets sprites of traffic light. If the value of 'sprites' is null or it's longer/shorter than 3,
		 * method will sets default ones.
		 * 
		 * @param sprites
		 * 			  Images to be set [Green, Yellow, Red]
		 * @throws IOException
		 * 			  if the application couldn't retrieve images
		 */
		private void setSprites(final Image[] sprites) throws IOException {
			if (sprites != null && sprites.length == 3) {
				this.sprites = sprites;
			} else {
				this.sprites = new BufferedImage[3];
				this.sprites[GREEN_INDEX] = ImageIO.read(new File(DEFAULT_SPRITES[GREEN_INDEX]));
				this.sprites[YELLOW_INDEX] = ImageIO.read(new File(DEFAULT_SPRITES[YELLOW_INDEX]));
				this.sprites[RED_INDEX] = ImageIO.read(new File(DEFAULT_SPRITES[RED_INDEX]));
			}
		}

		/**
		 * Returns the time given to the traffic light for the green time's
		 * state.
		 * 
		 * @return the time given to the traffic light for the green time's
		 *         state
		 */
		public Integer getGreenTime() {
			Integer value = this.greenTime != null ? this.greenTime : Roads.TrafficLight.DEFAULT_GREEN;
			return value;
		}

		/**
		 * Sets the time given to the traffic light for its green time's state.
		 * <p>
		 * Notes: leaves the default value if the time passed as parameter is
		 * less/equal to 0 or higher than 20 (this implementation prevents a
		 * crash of the application, used temporary in the 'beta' version).
		 * <p>
		 * 
		 * @param time
		 *            the time given for the green time's state
		 * 
		 */
		public void setGreenTime(final Integer time) {
			if (time <= 0 || time > MAX_TIME) {
				Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, 
						"Value for the green time of the traffic light is invalid, it has given the default one");
			} else {
				this.greenTime = time;
			}
		}

		/**
		 * Returns the turn of the traffic light inside the cross.
		 * 
		 * @return the turn of the traffic light inside the cross
		 */
		public int getPriority() {
			return this.priority;
		}

		/**
		 * Sets the turn of the traffic light inside the cross in which is
		 * involved.
		 * 
		 * @param priority
		 *            the turn of the traffic light
		 * 
		 * @throws IllegalArgumentException
		 *             if the value is less or equal than 0
		 */
		public void setPriority(final int priority) throws IllegalArgumentException {
			if (priority <= 0) {
				Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Value for the turn in the cross is invalid");
				throw new IllegalArgumentException();
			}

			this.priority = priority;
		}

		/**
		 * Returns the current state of the traffic light.
		 * 
		 * @return the current state of the traffic light
		 */
		public int getColour() {
			return this.colour;
		}

		/**
		 * Sets the current state of the traffic light.
		 * 
		 * @param colour
		 *            the new current state of the traffic light
		 * 
		 * @throws IllegalArgumentException
		 *             if the value passed as parameter is not in the range
		 */
		public void setColour(final int colour) throws IllegalArgumentException {
			if (colour != GREEN_INDEX && colour != YELLOW_INDEX && colour != RED_INDEX) {
				Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Value for the change color is invalid");
				throw new IllegalArgumentException();
			}

			this.colour = colour;
		}
		
		/**
		 * Sets a custom image for the related colour.
		 * 
		 * @param sprite the sprite to be setted as default image
		 * @param colour the colour to which the sprite will relate
		 */
		public void setSprite(final Image sprite, final int colour) {
			this.sprites[colour] = sprite;
		}
		
		/**
		 * Gets the sprite related to the actual colour.
		 * 
		 * @return the image portaying the traffic light in this moment
		 */
		public Image getSprite() {
			return this.sprites[this.colour];
		}

		/**
		 * Returns the string indicating the current state of the traffic light.
		 * 
		 * @return the string indicating the current state of the traffic light
		 */
		public String toString() {
			return this.position.toString() + " Traffic light's state: " + this.getColour();
		}
	}
}
