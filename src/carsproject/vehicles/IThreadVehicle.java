package carsproject.vehicles;

/**
 * Interface class for threads updater of models of vehicles.
 * This extends Runnable interface and include some thread's methods
 * because we are expecting that any class that will implements this
 * will also extends Thread or something similar.
 * It has been added only terminate() method to stop or pause vehicles.
 * 
 * @author Masini Gioele
 */
public interface IThreadVehicle extends Runnable {

	/**
	 * Every TIME_SLICE fractions of second it do a cycle scanning in front
	 * of itself to check if there are some obstacles.Then it move according
	 * to it's speed and road under it. This method is the heart of this
	 * class.
	 * Never run a vehicle from this method. Use instead start() and stop().
	 */
	public void run();

	/**
	 * This is a thread class, so it need to be run to work.
	 * This method will use Thread.run() with some other stuff.
	 */
	public void start();

	/**
	 * This method will stop the thread.
	 * Use this for both pause and stop a vehicle.
	 */
	public void terminate();

	/**
	 * Standard method to wait a thread termination.Added here to be sure
	 * that any implementation of this interface will have this method.
	 * 
	 * @throws InterruptedException following Thread declaration
	 */
	public void join() throws InterruptedException;
}
