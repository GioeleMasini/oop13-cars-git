package carsproject.vehicles;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import carsproject.general.Amanuensis;
import carsproject.general.Coordinates;
import carsproject.map.Direction;
import carsproject.map.ITile;
import carsproject.map.MapTiles;
import carsproject.map.Roads;
import carsproject.map.Roads.Bow;
import carsproject.map.Roads.Cross;
import carsproject.map.Roads.TrafficLight;
import carsproject.map.TileType;

/**
 * Implementation of IThreadVehicle with the task to move a "car" vehicle.
 * This need a ModelCar to retrieve and update informations about the vehicle.
 * This is one of the possible implementations to move a vehicle.
 * 
 * @author Masini Gioele
 */
public class ThreadCar extends AbstrThreadVehicle {

	/** Pixels to be skipped in scan methods. */
	//private static final Integer PIXELS_TO_SKIP = 6;
	/** Meters to be scanned by methods. */
	//private static final Integer METERS_TO_SCAN = 10;
	/** Speed of vehicles when curving or doing something strange. */
	private static final Double SPEED_WHILE_CARE = 30.0;
	/** Standard speed of cars. */
	private static final Double SPEED_DEFAULT = 50.0;
	
	/** Contains true if thread is running, false otherwise. */
	private boolean alive = false;
	/** Contains the position where the vehicle need to have reached speedToReach speed value. */
	private Coordinates speedToReachPosition = null;
	/** Contains the coordinates of the point around the vehicle have to curve. */
	private Coordinates centerToCurve = null;
	/** Reference to tile at last position of the vehicle (could be equal to actual one). */
	private ITile oldTile = null;
	
	
	public ThreadCar(final IModelVehicle myVehicle) {
		super(myVehicle);
		this.alive = false;
	}
	
	@Override
	public void start() {
		this.alive = true;
		super.start();
	}
	
	/**
	 * Stop the vehicle's thread. This simply check a boolean flag.
	 * To know if the thread is running or not use isRunning() method.
	 */
	public void terminate() {
		if (!Coordinates.isLegal(this.getModelVehicle().getPosition())) {
			List<Roads> occTiles = new ArrayList<>(this.getModelVehicle().getOccupiedTiles());
			for (Roads t : occTiles) {
				this.getModelVehicle().removeOccupiedTile(t);
				t.setFree(this.getModelVehicle().getName());
			}
		}
		this.getModelVehicle().setThread(null);
		this.alive = false;
	}

	/**
	 * Every TIME_SLICE fractions of second this do a cycle scanning in
	 * front of itself to check if there are some obstacles.
	 * Then it move according to it's speed and tile under it.
	 * This method is the heart of that class.
	 * Never run a vehicle from this method. Use instead start() and terminate().
	 */
	@Override
	public void run() {
		IModelVehicle myVehicle = this.getModelVehicle();
		Direction myWay = null;
		ITile lastTile = null;
		
		try {
			while (alive) {
				long start = System.currentTimeMillis();
				//System.out.println(myVehicle.getName() + " is at position " + myVehicle.getPosition() + " at speed " + myVehicle.getSpeed());
				Coordinates myPosition = myVehicle.getPosition();
				myWay = myVehicle.getWay() == null? myWay : myVehicle.getWay();
				myVehicle.setPixelsToGo(AbstrModelVehicle.kmPerHoursToPixelPerTimeSlice(myVehicle.getSpeed()));
				
				//Checking my tile
				ITile myTile = MapTiles.getInstance().getTileAt(myPosition);
				
				//Checking if there are some tiles to be occupied
				List<Roads> tilesToOccupy = null;
 				if (lastTile == null || myTile == null || myTile != lastTile) {
					List<Roads> toOccupy = ThreadCar.tilesUsedToGo(myWay, myVehicle.getPosition());
					if (toOccupy != null) {
						if (myVehicle.getOccupiedTiles().isEmpty()) {
							//If i've been spawned now, vehicle occupies necessary tiles
							int n = this.waitAndOccupyTiles(toOccupy, myVehicle.getName());
							for (Roads t : toOccupy) {
								myVehicle.addOccupiedTile(t);
							}
							if (n > 0) {
								myVehicle.setSpeed(0d);
								myVehicle.setSpeedToReach(SPEED_DEFAULT);
							}
						} else {
							if (myTile.getType() != TileType.CROSS && ThreadCar.getNextTileOnPath(myWay, myTile) != null
									&& ThreadCar.getNextTileOnPath(myWay, myTile).getType() == TileType.CROSS) {
								//Vehicle has to turn at next tile
								//Choosing a direction to turn and setting tiles to be occupied
								Direction futureWay = this.setCrossMovement(myTile, myVehicle);
								@SuppressWarnings("unchecked")	// Suppressed because Cross extends Roads and those tiles are useful only to keep trace of occupation
								List<Roads> tilesToTurn = (List<Roads>)(List<?>) ThreadCar.tilesUsedToTurn(myWay, futureWay, myTile);
								toOccupy.addAll(tilesToTurn);
								
								//Adding some more tiles of the future road. Vehicle will not turn if the road is already occupied.
								Roads firstTile = (Roads) ThreadCar.getNextTileOnPath(futureWay, tilesToTurn.get(tilesToTurn.size() - 1));
								Roads secondTile = (Roads) ThreadCar.getNextTileOnPath(futureWay, firstTile);
								toOccupy.add(firstTile);
								toOccupy.add(secondTile);
							}
						}
					}
					tilesToOccupy = toOccupy;
 				}
 				if (lastTile != null && lastTile != myTile) {
					//Clearing the list from no more occupied tiles
					if (myVehicle.isOccupiedTile((Roads) this.oldTile)) {
						((Roads) this.oldTile).setFree(myVehicle.getName());
						myVehicle.removeOccupiedTile(((Roads) this.oldTile));
					}
					
 				}
				
				//Switch done only one time for every tile
				if (myTile != lastTile) {
					switch(myTile.getType()) {
					case ROAD:	{
						//Rounding curvature to the next correct one
						if (myVehicle.getCurvatureAngleToReach() != myVehicle.getCurvatureAngleActual()
								&& Math.abs(myVehicle.getCurvatureAngleToReach() - myVehicle.getCurvatureAngleActual()) < Math.PI / 100) {
							myVehicle.setCurvatureAngleActual(myVehicle.getCurvatureAngleToReach());
						}
						//Repositioning vehicle at center
						if (this.oldTile != null && this.oldTile.getType() != TileType.ROAD){
							if (myWay == Direction.N || myWay == Direction.S) {
								myVehicle.setPosition(myTile.getPosition().getX() + (ITile.TILE_EDGE / 2), myVehicle.getPosition().getY());
							} else if (myWay == Direction.W || myWay == Direction.E) {
								myVehicle.setPosition(myVehicle.getPosition().getX(), myTile.getPosition().getY() + (ITile.TILE_EDGE / 2));
							}
							myPosition = myVehicle.getPosition();
						}
						Integer n = this.waitAndOccupyTiles(tilesToOccupy, myVehicle.getName());
						if (n > 0) {
							//If the vehicle has waited, speed will be set at 0
							myVehicle.setSpeed(0d);
							myVehicle.setSpeedToReach(SPEED_DEFAULT);
						}
						for (Roads t : tilesToOccupy) {
							myVehicle.addOccupiedTile(t);
						}
						break;
					}
					case BOW:	{
						Double myCurvature = myVehicle.getCurvatureAngleActual();
						if (!(myTile.equals(lastTile))) {
							//First time in this bow, I have to start my curve
							
							Direction[] ways = ((Bow) myTile).getPossibleWays();
							
							if (myWay != null) {
								//Calculating final way
								if (ways[0] != myWay && ways[1] != myWay) {
									ways[0] = Direction.getContrary(ways[0]);
									ways[1] = Direction.getContrary(ways[1]);
								}
								Direction myFutureWay = (ways[0] == myWay ? ways[1] : ways[0]);

								myVehicle.setCurvatureAngleToReach(myCurvature + ThreadCar.directionsToFinalCurvature(myWay, myFutureWay));
							}
						}
						Integer n = this.waitAndOccupyTiles(tilesToOccupy, myVehicle.getName());
						if (n > 0) {
							//If the vehicle has waited, speed will be set at 0
							myVehicle.setSpeed(0d);
							myVehicle.setSpeedToReach(SPEED_DEFAULT);
						}
						for (Roads t : tilesToOccupy) {
							myVehicle.addOccupiedTile(t);
						}
						break;
					}
					case STOP: {
						myVehicle.setSpeed(0.0);
						ThreadCar.sleep(800);		//Trick to make vehicle stop, simulating a real movement
						
						Integer n = this.waitAndOccupyTiles(tilesToOccupy, myVehicle.getName());
						if (n > 0) {
							//If the vehicle has waited, speed will be set at 0
							myVehicle.setSpeed(0d);
						}
						for (Roads t : tilesToOccupy) {
							myVehicle.addOccupiedTile(t);
						}
						
						myVehicle.setSpeedToReach(SPEED_DEFAULT);
						this.speedToReachPosition = null;
						break;
					}
					case PRIORITY: {
						myVehicle.setSpeed(SPEED_WHILE_CARE);
						
						Integer n = this.waitAndOccupyTiles(tilesToOccupy, myVehicle.getName());
						if (n > 0) {
							//If the vehicle has waited, speed will be set at 0
							myVehicle.setSpeed(0d);
						}
						for (Roads t : tilesToOccupy) {
							myVehicle.addOccupiedTile(t);
						}
						
						myVehicle.setSpeedToReach(SPEED_DEFAULT);
						this.speedToReachPosition = null;
						break;
					}
					case TRAFFIC_LIGHT: {
						TrafficLight temp = (TrafficLight) myTile;
						do {
							for (Roads t : tilesToOccupy) {
								myVehicle.removeOccupiedTile(t);
								t.setFree(myVehicle.getName());
							}
							
							//Waiting green on semaphore
							while (temp.getColour() != TrafficLight.GREEN_INDEX) {
								myVehicle.setSpeed(0d);
								sleep(300);
							}
							
							//Trying to occupy the cross
							Integer n = this.waitAndOccupyTiles(tilesToOccupy, myVehicle.getName());
							if (n > 0) {
								//If the vehicle has waited, speed will be set at 0
								myVehicle.setSpeed(0d);
							}
							for (Roads t : tilesToOccupy) {
								myVehicle.addOccupiedTile(t);
							}
							
						} while (temp.getColour() != TrafficLight.GREEN_INDEX);
						
						myVehicle.setSpeedToReach(SPEED_DEFAULT);
						break;
					}
					default: {
						//Case of ZEBRA_CROSSING, not implemented.
						//Case of ROAD, BASE and CROSS tiles. Nothing to do.
						break; }
					}
				}
				
				myVehicle.setPixelsToGo(AbstrModelVehicle.kmPerHoursToPixelPerTimeSlice(myVehicle.getSpeed()));
				
				//Doing the necessary movements (curve or go straight)
				if (myVehicle.getCurvatureAngleActual() != myVehicle.getCurvatureAngleToReach() && (myTile.getType() == TileType.CROSS || myTile.getType() == TileType.BOW)) {
						//Calculating center of the curve for bow
						if (myTile.getType() == TileType.BOW) {
							this.centerToCurve = ((Bow) myTile).getCenterBow();
						}
						Double myCurvature = myVehicle.getCurvatureAngleActual();
						Double curvatureToReach = myVehicle.getCurvatureAngleToReach();
						Double radius = Coordinates.distanceFromTwoPoints(this.centerToCurve, myPosition);
						myVehicle.setPosition(this.moveCurve(myPosition, this.centerToCurve, myCurvature, myVehicle.getSpeed(), radius, curvatureToReach, myVehicle.getPixelsToGo()));
						Double newCurvature = this.curveAngle(Math.min(AbstrModelVehicle.kmPerHoursToPixelPerTimeSlice(myVehicle.getSpeed()), myVehicle.getPixelsToGo()), radius);
						newCurvature = myCurvature < curvatureToReach ? newCurvature : -newCurvature;
						newCurvature += myCurvature;
						if (myCurvature <= curvatureToReach && newCurvature > curvatureToReach) {
							myVehicle.setCurvatureAngleActual(curvatureToReach);
						} else if (myCurvature >= curvatureToReach && newCurvature < curvatureToReach) {
							myVehicle.setCurvatureAngleActual(curvatureToReach);
						} else {
							myVehicle.setCurvatureAngleActual(newCurvature);
						}
				} else {
					myVehicle.setPosition(this.moveStraight(myPosition, myVehicle.getSpeed(), myVehicle.getCurvatureAngleActual(), myVehicle.getPixelsToGo()));
					if (lastTile != null && myTile != MapTiles.getInstance().getTileAt(myPosition)) {
						if (lastTile.getType() == TileType.CROSS || lastTile.getType() == TileType.BOW) {
							this.centerToCurve = null;
						}
					}
				}
				
				//Checking if with new coordinates the vehicle is still in the map
 				if (!Coordinates.isLegal(myVehicle.getPosition()) || MapTiles.getInstance().getTileAt(myVehicle.getPosition()) == null) {
					//If out of map the vehicle kill itself
					//System.out.println(this.toString() + " gone out of map and is destroyed;");
					this.terminate();
				}
				
				//Accelerating/Decelerating if necessary
				if (myVehicle.getSpeed() != myVehicle.getSpeedToReach()) {
					if (this.speedToReachPosition == null) {
						myVehicle.setSpeed(this.accelerate(myVehicle.getSpeed(), myVehicle.getSpeedToReach()));
					} else {
						myVehicle.setSpeed(this.accelerate(myVehicle.getSpeed(), myVehicle.getSpeedToReach(), myVehicle.getPosition(), this.speedToReachPosition));
					}
				} else {
					this.speedToReachPosition = null;
				}
				
				if (lastTile != myTile) {
					this.oldTile = lastTile != null ? MapTiles.getInstance().getTileAt(lastTile.getPosition()) : null;
				}
				lastTile = myTile;
				
				//Calculating spent time for last cycle
				long spentTime = System.currentTimeMillis() - start;
				
				//And sleeping remaining time before the end of the time slice
				try {
					Thread.sleep(IModelVehicle.TIME_SLICE - spentTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Thread of vehicle " + this + " cannot sleep. Movement cycle has been too slow.");
				}
			}
		} catch (Exception e) {
			Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Thread " + this.getId() + " of car '" + this.getModelVehicle().getName() + "' is stopped.\nCause: " + e);
			System.out.println("Destroied car " + this.getModelVehicle().getName() + ". Cause: " + e);
			e.printStackTrace();
			this.terminate();
			this.getModelVehicle().setPosition(new Coordinates(Coordinates.OUT_OF_MAP, Coordinates.OUT_OF_MAP));
		}
	}
	
	/**
	 * Calculates curvature radians from two given directions.
	 * 
	 * @param from Actual direction of the vehicle
	 * @param to Future direction of the vehicle
	 * @return Rotational radians to be curved
	 */
	private static Double directionsToFinalCurvature(final Direction from, final Direction to) {
		Integer nFrom = Direction.wayToNumber(from);
		Integer nTo = Direction.wayToNumber(to);
		
		Double rotDegrees = (nTo - nFrom) * 90d;
		if (to == Direction.S || from == Direction.S) {
			rotDegrees *= -1;
		}
		return Math.toRadians(rotDegrees);
	}
	
	/**
	 * Try to occupies a list of tiles to let the vehicle move on it.
	 * Returns true if all gone right, false otherwise.
	 * 
	 * @param tiles list of tiles to occupy
	 * @param vehicleName name of the vehicle that will occupy them
	 * @return true if the list has been correctly occupied, false if was already busy.
	 */
	private static boolean occupiesTiles(final List<Roads> tiles, String vehicleName) {
		List<Roads> occupied = new ArrayList<>();
		boolean success = true;
		Iterator<Roads> i = tiles.iterator();
		//Trying to occupy all crosses
		while (i.hasNext() && success) {
			Roads t = i.next();
			success = occupyTile(t, vehicleName);
			if (success) {
				occupied.add(t);
			}
		}
		
		//If something gone wrong with a tile, need to set occupied as free
		if (!success) {
			for (Roads t : occupied) {
				t.setFree(vehicleName);
			}
		}
		return success;
	}
	
	/**
	 * Synchronized occupation of one tile with a road.
	 * 
	 * @param tile "Roads" to occupy
	 * @param vehicleName name of the vehicle that will occupy that tile
	 * @return true if the tile has been correctly occupied, false if was already busy
	 */
	private static synchronized boolean occupyTile(Roads tile, String vehicleName) {
			if (!tile.isOccupied()) {
				//If tile is free, method will occupy it and return true
				tile.setOccupied(vehicleName);
				return true;
			} else if (tile.getOccupyingVehicle() == vehicleName){
				//If tile has already been occupied by my vehicle, return true
				return true;
			}

			//If the tile is already occupied, return false
			return false;
	}
	
	/**
	 * Wait until the cross is free and then occupy it and exit.
	 * 
	 * @param tilesToOccupy list of the cross tiles needed by vehicle
	 * @param vehicleName name of the vehicle that will occupy those tiles
	 * @return the number of cycle the vehicle has waited (every cycle last for a time slice).
	 */
	private int waitAndOccupyTiles(final List<Roads> tilesToOccupy, String vehicleName) {
		boolean crossOccupied = false;
		int n = -1;
		
		while (!crossOccupied) {
			n++;
			//Trying to occupy the cross
			crossOccupied = ThreadCar.occupiesTiles(tilesToOccupy, vehicleName);
			
			if (!crossOccupied) {
				//Waiting a "time slice" before next try
				try { 
					Thread.sleep(IModelVehicle.TIME_SLICE);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		return n;
	}
	
	/**
	 * Return a list of tiles needed to continue straight on the road. Those will have to be occupied.
	 * This will stop to check for other tiles if find a Cross.
	 * 
	 * @param actualWay direction of the vehicle
	 * @param vehicleCoords coordinates of the vehicle (center)
	 * @return list of tiles that should be occupied in order to continue to move,
	 * 		null if vehicle is on a CROSS because method can't know where it will go
	 */
	private static List<Roads> tilesUsedToGo(final Direction actualWay, final Coordinates vehicleCoords) {
		ITile t = MapTiles.getInstance().getTileAt(vehicleCoords);
		if (t == null || t.getType() == TileType.CROSS) {
			return null;
		}
		if (t.getType().equals(TileType.BASE)) {
			throw new IllegalStateException("Vehicle on a BASE tile. Fatal error.");
		}
		
		Roads myTile = (Roads) t;
		List<Roads> list = new LinkedList<>();
		
		//Calculating next tile
		ITile nextT;
		Direction finalWay;
		if (myTile.getType() == TileType.BOW) {
			Direction[] pw = myTile.getPossibleWays();
			//Checking if possible ways are set good
			if (pw[0] != actualWay && pw[1] != actualWay) {
				pw[0] = Direction.getContrary(pw[0]);
				pw[1] = Direction.getContrary(pw[1]);
			}
			
			//Taking the direction after curve
			if (actualWay == pw[0]) {
				finalWay = pw[1];
			} else {
				finalWay = pw[0];
			}
			
			//Asking for next tile with that finalWay
			nextT = ThreadCar.getNextTileOnPath(finalWay, myTile);
		} else {
			nextT = ThreadCar.getNextTileOnPath(actualWay, myTile);
		}
		
		//Calculating last tile
		/*ITile lastT = ThreadCar.getNextTileOnPath(Direction.getContrary(actualWay), myTile);
		
		//Checking if last tile has to be added to list
		if (lastT != null && lastT.getType() != TileType.BASE && lastT.getType() != TileType.CROSS) {
			list.add((Roads) lastT);
		}*/
		//Adding actual tile
		list.add(myTile);
		//Checking if next tile has to be added to list
		if (nextT != null && nextT.getType() != TileType.BASE && nextT.getType() != TileType.CROSS) {
			list.add((Roads) nextT);
		}
		
		return list;
	}

	/**
	 * Return a list with cross tiles needed to make the movement from actualWay to futureWay.
	 * 
	 * @param actualWay direction of the vehicle
	 * @param futureWay direction at the end of the movement
	 * @param startTile starting tile (tile before the cross, like Stop or Priority)
	 * @return the list of cross tiles used by the movement.
	 * @throws IllegalArgumentException the method support only directions N, S, E and W. With others will throw an Exception.
	 */
	private static List<Cross> tilesUsedToTurn(final Direction actualWay, final Direction futureWay, final ITile startTile)
			throws IllegalArgumentException {
		List<Cross> tiles = new ArrayList<Cross>();
		String errorString = "Something gone wrong. Program support only N, S, E and W directions.";
		
		//Calculating how many tiles have to be listed
		Integer nTiles = 0;
 		if (actualWay.equals(futureWay)) {
			nTiles = 2;
		} else {
			switch (actualWay) {
				case N: {
					if (futureWay.equals(Direction.E)) {
						nTiles = 1;
					} else if (futureWay.equals(Direction.W)) {
						nTiles = 3;
					} else {
						//Other cases not supported;
						throw new IllegalArgumentException(errorString);
					}
					break;
				}
				case S: {
					if (futureWay.equals(Direction.E)) {
						nTiles = 3;
					} else if (futureWay.equals(Direction.W)) {
						nTiles = 1;
					} else {
						//Other cases not supported;
						throw new IllegalArgumentException(errorString);
					}
					break;
				}
				case W: {
					if (futureWay.equals(Direction.N)) {
						nTiles = 1;
					} else if (futureWay.equals(Direction.S)) {
						nTiles = 3;
					} else {
						//Other cases not supported;
						throw new IllegalArgumentException(errorString);
					}
					break;
				}
				case E: {
					if (futureWay.equals(Direction.N)) {
						nTiles = 3;
					} else if (futureWay.equals(Direction.S)) {
						nTiles = 1;
					} else {
						//Other cases not supported;
						throw new IllegalArgumentException(errorString);
					}
					break;
				}
				default: {
					//Other cases not supported;
					throw new IllegalArgumentException(errorString);
				}
			}
		}
		
		
		//Calculating and adding first cross tile
		ITile firstCrossTile = ThreadCar.getNextTileOnPath(actualWay, startTile);		
		tiles.add((Cross) firstCrossTile);
		nTiles--;
		
		if (nTiles == 0) {
			return tiles;
		}
		
		//Calculating differences between X and Y position between startTile and firstCrossTile
		Coordinates c = new Coordinates(firstCrossTile.getPosition().getX(), firstCrossTile.getPosition().getY());
		Integer adderX = c.getX() - startTile.getPosition().getX();
		Integer adderY = c.getY() - startTile.getPosition().getY();
		
		
		//Calculating and adding second cross tile
		c.addX(adderX);
		c.addY(adderY);
		tiles.add((Cross) MapTiles.getInstance().getTileAt(c));
		nTiles--;
		
		if (nTiles == 0) {
			return tiles;
		}
		
		//Calculating and adding third cross tile
		if (actualWay.equals(Direction.E) || actualWay.equals(Direction.W)) {
			adderY *= -1;
			adderX *= -1;
		}
		c.addX(adderY);
		c.addY(adderX);
		tiles.add((Cross) MapTiles.getInstance().getTileAt(c));
		
		return tiles;
	}
	
	/**
	 * Select a random way to turn and set variables that will be used to make the movement.
	 * 
	 * @param vTile tile of the vehicle
	 * @param vModel model of the vehicle
	 * @return direction the vehicle will go
	 */
	private Direction setCrossMovement(final ITile vTile, final IModelVehicle vModel) {
		Cross firstCrossTile = (Cross) ThreadCar.getNextTileOnPath(vModel.getWay(), vTile);
		Direction[] d = firstCrossTile.getPossibleWays();
		Integer rand = (int) (Math.random() * (d.length));
		Direction wayToTurn = d[rand];
		
		//Calculating radians of rotation
		Double curveRadians = ThreadCar.directionsToFinalCurvature(vModel.getWay(), wayToTurn);
		vModel.setCurvatureAngleToReach(vModel.getCurvatureAngleActual() + curveRadians);

		//Calculating and setting center of the curve							
		this.centerToCurve = AbstrThreadVehicle.getCenterPoint(firstCrossTile, vModel.getWay(), wayToTurn);
		
		return wayToTurn;
	}
	
	/**
	 * Return the next tile the vehicle will find going straight on its way.
	 * 
	 * @param actualWay direction of the vehicle
	 * @param actualTile tile under the vehicle
	 * @return next tile on the direction of the vehicle
	 * @throws IllegalArgumentException when a direction different from N, S, W and E is passed as input.
	 * 			Other directions are not supported.
	 */
	private static ITile getNextTileOnPath(final Direction actualWay, final ITile actualTile) throws IllegalArgumentException {
		if (actualWay == null || actualTile == null) {
			return null;
		}
		
		Integer adderX = 0;
		Integer adderY = 0;
		switch (actualWay) {
			case N: {
				adderY = -ITile.TILE_EDGE;
				break;
			}
			case S: {
				adderY = +ITile.TILE_EDGE;
				break;
			}
			case W: {
				adderX = -ITile.TILE_EDGE;
				break;
			}
			case E: {
				adderX = +ITile.TILE_EDGE;
				break;
			}
			default: {
				//Other cases not supported.
				throw new IllegalArgumentException("Method support only N, S, W and E directions.");
			}
		}

		Coordinates c = new Coordinates(actualTile.getPosition().getX() + adderX, actualTile.getPosition().getY() + adderY);
		return MapTiles.getInstance().getTileAt(c);
	}
	
	/**
	 * Check if a vehicle is spawnable at input coordinates.
	 * 
	 * @param c coordinates to check
	 * @return true if is spawnable, false if not (tiles already occupied or not roads)
	 */
	static public boolean isSpawnableAt(Coordinates c) {
		ITile t = MapTiles.getInstance().getTileAt(c);
		//Checking if tile is a road in the map
		if (t == null || t.getType() == TileType.BASE || ((Roads) t).isOccupied()) {
			return false;
		}
		
		Direction way = t.getPossibleWays()[0];
		
		//Obtaining needed tiles and checking if they are free
		Roads nextT = (Roads) ThreadCar.getNextTileOnPath(way, t);
		if (nextT != null && nextT.isOccupied()) {
			return false;
		}
		
		nextT = (Roads) ThreadCar.getNextTileOnPath(Direction.getContrary(way), t);
		if (nextT != null && nextT.isOccupied()) {
			return false;
		}
		return true;
	}
}
