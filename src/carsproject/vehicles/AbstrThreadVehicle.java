package carsproject.vehicles;

import carsproject.general.Amanuensis;
import carsproject.general.Coordinates;
import carsproject.map.Direction;
import carsproject.map.ITile;
import carsproject.map.MapTiles;
import carsproject.map.TileType;

/**
 * Abstract class for vehicle's threads.
 * Provide no methods of interface (excluding those inherited its super class Thread)
 * but instead contains functions that can help programmers in future final implementations
 * of the method run(), which has the task to move the vehicle.
 * They can do mathematical calculations to retrieve updated coordinates of a vehicle
 * moving straight or curving, to accelerate/decelerate until a new speed and to scan
 * in front of itself in search of other vehicles or important tiles
 * (all tiles that aren't simply straight roads).
 * 
 * @author Masini Gioele
 */
public abstract class AbstrThreadVehicle extends Thread implements IThreadVehicle {
	/**
	 * Reference to model class, used to store vehicle's data.
	 */
	private IModelVehicle myVehicle = null;
	
	/**
	 * Constant of standard acceleration
	 */
	static final Double ACCELERATION = 4.0;
	
	/**
	 * Base constructor. Will set thread of vehicle passed as input.
	 * 
	 * @param myVehicle vehicle to be updated from this thread
	 */
	public AbstrThreadVehicle(final IModelVehicle myVehicle) {
		this.myVehicle = myVehicle;
		myVehicle.setThread(this);
	}
	
	/**
	 * This method must be used to start the thread. This because we want
	 * the possibility to have a vehicle not already in the map or paused.
	 */
	public abstract void terminate();
	

	/**
	 * This is an extends-thread class, so this is needed to start the thread.
	 * If you want to override always use super method.
	 */
	public void start() {
		super.start();
	}
	
	/**
	 * This method find next important tile in front of the vehicle.
	 * Important tiles are not BASE and ROAD.
	 * 
	 * @param scanFromPosition position from where this method will scan
	 * @param metersToScan meters to be scanned
	 * @param pixelsToSkip pixels to be skipped every scan
	 * @return ITile if founded an important tile, null otherwise
	 */
	protected ITile findNextTile(final Coordinates scanFromPosition, final Integer metersToScan, final Integer pixelsToSkip) {
		try {
		for (int i = 0; i < metersToScan * MapTiles.PIXEL_ON_METER; i += pixelsToSkip) {
			Coordinates tempPos = this.moveStraightPoint(myVehicle.getPosition(), i + 0.0, myVehicle.getCurvatureAngleActual());
			ITile tempTile = MapTiles.getInstance().getTileAt(tempPos);
			if (tempTile != MapTiles.getInstance().getTileAt(myVehicle.getPosition())) {
				if (tempTile.getType() != TileType.BASE && tempTile.getType() != TileType.ROAD) {
					//Found an important tile
					return tempTile;
				}
			}
		}
		} catch (Exception e) {
			Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Scansion of new tiles terminated because gone out of map.");
		}
		//No important tiles in those meters
		return null;
	}
	
	/**
	 * Scan the path in front of the vehicle in search of another vehicle.
	 * If founded this will return it.
	 * 
	 * @param scanFromPosition position from where this method will scan
	 * @param metersToScan meters to be scanned
	 * @param pixelsToSkip pixels to be skipped every scan
	 * @return IThreadVehicle if found a vehicle near this, null otherwise.
	 */
	protected IModelVehicle findNextVehicle(final Coordinates scanFromPosition, final Integer metersToScan, final Integer pixelsToSkip) {
		try {
			for (int i = 0; i < metersToScan * MapTiles.PIXEL_ON_METER; i += pixelsToSkip) {
				Coordinates tempPos = this.moveStraightPoint(myVehicle.getPosition(), i + 0.0, myVehicle.getCurvatureAngleActual());
				IModelVehicle tempVehicle = Garage.getInstance().getVehicle(tempPos);
				if (tempVehicle != null) {
					//Found a vehicle
					return tempVehicle;
				}
			}
		} catch (Exception e) {
			//Scansion gone out of map, but there is no problem.
		}
		//No vehicles founded in those meters
		return null;
	}
	
	/**
	 * Returns the new position of a moving straight vehicle.
	 * It returns the final position, after the movement,
	 * and update the variable movementsAvaiable subtracting pixel used to move.
	 * 
	 * @param startPoint position of the vehicle
	 * @param speed speed of the vehicle, in km/h
	 * @param angle rotation angle of the vehicle, in radians
	 * @param movementsAvaiable vehicle's pixels that it can still move
	 * @return the final position after the movement
	 */
	protected Coordinates moveStraight(Coordinates startPoint, final Double speed, final Double angle, Double movementsAvaiable) {
		Double radius = Math.min(AbstrModelVehicle.kmPerHoursToPixelPerTimeSlice(speed), movementsAvaiable);

		//Setting all variables to the right value
		startPoint = this.moveStraightPoint(startPoint, radius, angle);
		movementsAvaiable -= radius;
		return startPoint;
	}
	
	/**
	 * Private method used to calculate the final position of
	 * a vehicle moving straight.
	 * 
	 * @param startPoint position of the vehicle
	 * @param radius pixels to paths by vehicle
	 * @param angle rotation angle of the vehicle, in radians
	 * @return the new position after the movement
	 */
	private Coordinates moveStraightPoint(final Coordinates startPoint, final Double radius, final Double angle) {
		Coordinates endAngle = new Coordinates(0, 0);
		endAngle.addX(startPoint.getX() + radius * Math.cos(angle));
		endAngle.addY(startPoint.getY() + radius * Math.sin(angle));
		return endAngle;
	}
	
	/**
	 * Private method used to calculate final position after a curve movement.
	 * This also update movementsAvaiable considering pixels moved
	 * in because of this method.
	 * 
	 * @param startPoint position of the vehicle
	 * @param startAngle rotation angle of the vehicle, in radians
	 * @param speed speed of the vehicle, in km/h
	 * @param radius radius of the curve.
	 * 			It have to be calculated from center of the curve to the vehicle
	 * @param maxEndAngle max rotation angle vehicle can assume after
	 * 			the movement, in radians
	 * @param movementsAvaiable  vehicle's pixels that it can still move
	 * @return the final position after the movement
	 */
	protected Coordinates moveCurve(Coordinates startPoint, final Coordinates centerPoint, Double startAngle, final Double speed, final Double radius, final Double maxEndAngle, Double movementsAvaiable) {
		Double arc = Math.min(AbstrModelVehicle.kmPerHoursToPixelPerTimeSlice(speed), movementsAvaiable);
		boolean clockwise = startAngle < maxEndAngle ? true : false;
		
		//Calculating angle (and eventually modification of arc)
		Double endAngle = this.curveAngle(arc, radius);
		endAngle = startAngle < maxEndAngle ? endAngle : -endAngle;
		endAngle += startAngle;
		if ((startAngle <= maxEndAngle && endAngle > maxEndAngle) || (startAngle >= maxEndAngle && endAngle < maxEndAngle)) {
			endAngle = maxEndAngle;
			arc = radius * startAngle;
		}
		
		//Setting all variables to the right value
		Coordinates endPoint = moveCurvePoint(startPoint, centerPoint, radius, arc, endAngle, clockwise);
		movementsAvaiable -= arc;
		
		return endPoint;
	}
	
	/**
	 * Private method used to calculate final position after a curving movement.
	 * 
	 * @param startPoint position of the vehicle
	 * @param radius radius of the curve.
	 * 				It's to be calculated from curve's center to the vehicle
	 * @param arc pixels to paths by vehicle
	 * @return the new position after the movement
	 */
	private Coordinates moveCurvePoint(final Coordinates startPoint, final Coordinates centerPoint, final Double radius, final Double arc, Double angle, final boolean clockwise) {
		if (angle == null) {
			angle = this.curveAngle(arc, radius);
		}
		Coordinates newCoords = new Coordinates(0, 0);
		if (clockwise) {
			angle -= Math.PI / 2;	//shrewdness to have horizontal vehicle at curvature zero
		} else {
			angle += Math.PI / 2;
		}
		newCoords.setX(Math.cos(angle) * radius + centerPoint.getX());
		newCoords.setY(Math.sin(angle) * radius + centerPoint.getY());
		return newCoords;
	}
	
	/**
	 * Given radius and arc of the curve it calculate angle done and return it.
	 * 
	 * @param arc pixels to paths by vehicle
	 * @param radius radius of the curve.
	 * 				It have to be calculated from curve's center to the vehicle
	 * @return angle done in radians
	 */
	protected Double curveAngle(final Double arc, final Double radius) {
		return (arc / radius);
	}
	
	/**
	 * Accelerate or decelerate with a final speed and a point to be reach.
	 * This method will calculate acceleration with input values.
	 * It returns the new speed after an acceleration of a time slice.
	 * STILL NOT WORKING!!
	 * 
	 * @param actualSpeed speed of the vehicle
	 * @param speedToReach speed to reach accelerating/decelerating
	 * @param actualPosition position of the vehicle
	 * @param endPosition position to reach at the end of the
	 * 					acceleration/deceleration
	 * @return new speed after a time slice
	 */
	protected Double accelerate(final Double actualSpeed, final Double speedToReach, final Coordinates actualPosition, final Coordinates endPosition) {
		Double distance = Coordinates.distanceFromTwoPoints(actualPosition, endPosition);
		Double speedGap = actualSpeed - speedToReach;
		speedGap = speedGap < 0 ? -speedGap : speedGap;
		Double acceleration = AbstrModelVehicle.metersPerSecToKmPerHour(1 / 2 * Math.pow(actualSpeed, 2) / distance);
		
		//Don't know if this "if" is needed
		if (speedGap < 5 && distance < AbstrModelVehicle.kmPerHoursToPixelPerTimeSlice(actualSpeed)) {
			return 0.0;
		} else {
			//finalSpeed = acceleration*time + initialSpeed
			if (actualSpeed < speedToReach) {
				//Accelering
				return Math.min(acceleration * IModelVehicle.TIME_SLICE / 1000 + actualSpeed, speedToReach);
			} else {
				//Decelering
				return Math.max(-acceleration * IModelVehicle.TIME_SLICE / 1000 + actualSpeed, speedToReach);
			}
		}
	}
	
	/**
	 * Calculate new speed after acceleration with an average
	 * acceleration of 3.5.
	 * This method is used when a vehicle have to change its speed
	 * without a position as objective.
	 * Example: vehicle that restart after a STOP on a normal road.
	 * It must go at 50km/h with a 'normal' acceleration.
	 * 
	 * @param actualSpeed speed of the vehicle
	 * @param speedToReach speed to be reach at the end of the accelerate
	 * @return the new speed after accelerate
	 */
	protected Double accelerate(final Double actualSpeed, final Double speedToReach) {
		Double acceleration = AbstrModelVehicle.metersPerSecToKmPerHour(AbstrThreadVehicle.ACCELERATION * IModelVehicle.TIME_SLICE / 1000);
		if (actualSpeed < speedToReach) {
			//Accelering
			return Math.min(acceleration + actualSpeed, speedToReach);
		} else {
			//Decelering
			return Math.max(-acceleration + actualSpeed, speedToReach);
		}
	}
	
	/**
	 * Set model vehicle's object related to this thread.
	 * 
	 * @param modelVehicle model vehicle to be updated by this thread
	 */
	protected void setModelVehicle(final IModelVehicle modelVehicle) {
		this.myVehicle = modelVehicle;
	}
	
	/**
	 * Return model vehicle's object related to this thread.
	 * 
	 * @return object that this thread is updating
	 */
	public IModelVehicle getModelVehicle() {
		return this.myVehicle;
	}
	
	/**
	 * Function to get center point of a curve.
	 * 
	 * @param startTile Tile from which the vehicle will curve
	 * @param startDir Original direction of the vehicle
	 * @param endDir Final direction of the vehicle
	 * @return the point around which the vehicle will curve
	 */
	public static Coordinates getCenterPoint(final ITile startTile, final Direction startDir, final Direction endDir) {
		//Checking if the vehicle doesn't have to curve
		if (Math.abs(Direction.wayToNumber(startDir)) == Math.abs(Direction.wayToNumber(endDir))) {
			return null;
		}
		
		//Calculates center point
		Integer tileLength = ITile.TILE_EDGE;
		Integer x = startTile.getPosition().getX();
		Integer y = startTile.getPosition().getY();
		Coordinates centerPoint = null;
		
		if (startDir == Direction.W) {
			x += tileLength;
			switch(endDir) {
			case N: {
				centerPoint = new Coordinates(x, y);
				break;
			}
			case S: {
				centerPoint = new Coordinates(x, y + (2 * tileLength));
				break;
			}
			default: {
				centerPoint = null;
			}
			}
		} else if (startDir == Direction.N) {
			y += tileLength;
			switch(endDir) {
			case E: {
				centerPoint = new Coordinates(x + tileLength, y);
				break;
			}
			case W: {
				centerPoint = new Coordinates(x - tileLength, y);
				break;
			}
			default: {
				centerPoint = null;
			}
			}
		} else if (startDir == Direction.E) {
			switch(endDir) {
			case N: {
				centerPoint = new Coordinates(x, y - tileLength);
				break;
			}
			case S: {
				centerPoint = new Coordinates(x, y + tileLength);
				break;
			}
			default: {
				centerPoint = null;
			}
			}
		} else if (startDir == Direction.S) {
			switch(endDir) {
			case E: {
				centerPoint = new Coordinates(x + (2 * tileLength), y);
				break;
			}
			case W: {
				centerPoint = new Coordinates(x, y);
				break;
			}
			default: {
				centerPoint = null;
			}
			}
		}
		
		return centerPoint;
	}
}
