package carsproject.vehicles;

import java.lang.reflect.Constructor;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import carsproject.general.Amanuensis;
import carsproject.general.Coordinates;

/**
 * This class contains methods used to instantiate new vehicles
 * and to get info on those already instantiated.
 * This is Singleton, so use {@link Garage.getInstance}
 * to get his unique instance.
 * It is really important to remember that vehicle's name must be unique.
 * 
 * @author Masini Gioele
 *
 */
public final class Garage {
	/** Unique instance of Garage class, using singleton pattern. */
	private static final Garage SINGLETON = new Garage();
	/** Map of all vehicles created. Key is vehicle name and Value is vehicle model. */
	private Map<String, IModelVehicle> vehicles = new ConcurrentHashMap<>();
	
	/** Unaccessible constructor, following singleton pattern. */
	private Garage() {
	}
	
	/**
	 * Return the unique instance of Garage. It is singleton so constructor
	 * is unaccessible and this is the unique way to get its instance.
	 * 
	 * @return the unique instance of Garage
	 */
	public static Garage getInstance() {
		return Garage.SINGLETON;
	}

	/**
	 * Create a new vehicle giving vehicle's common name as first parameter
	 * and vehicle's unique name as second.
	 * Because of this method all implementations of IModelVehicle must be named
	 * as "Model_____" with the common name of vehicle in blank space.
	 * 
	 * @param vehicle type-name of the vehicle.
	 * 			Example: "ModelCar", where 'car' is the common name
	 * @param name unique name given to that new vehicle.
	 * @return the vehicle created, null if got some errors
	 */
	public IModelVehicle createVehicle(final String vehicle, final String name) {
		/*	Reflection method, that means that every vehicle class
		 	have to be called "Model____" with name of vehicle
			as second word, capitalized.
			Example: car vehicle is "ModelCar" */
		try {
			Constructor<?> c = Class.forName("carsproject.vehicles.Model" + this.capitalizeFirstLetter(vehicle)).getConstructor(String.class);
			IModelVehicle v = (IModelVehicle) c.newInstance(name);
			vehicles.put(name, v);
			return v;
		} catch (Exception e) {
			Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Failed to instantiate vehicle '" + vehicle + "' named '" + name + "';");
			Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, e.getMessage());
			return null;
		}
		 
		/* Evenctual method to use instead of reflection, but this need to be
		modified any times someone add another vehicle */
		/*switch (vehicle) {	
			case "car": {
				this.vehicles.add(new ModelCar(name));
				break;
			}
		}*/

	}
	
	/**
	 * Delete a vehicle giving its unique name.
	 * 
	 * @param name unique name of the vehicle to be deleted
	 */
	public void deleteVehicle(final String name) {
		if (name != null && this.vehicles.containsKey(name)) {
			this.vehicles.get(name).getThread().terminate(); // Asking to terminate
			this.vehicles.remove(name); // Removing from garage
		}
	}

	/**
	 * Internal function used to capitalize type-name of vehicle
	 * passed as input in createVehicle().
	 * It made first letter upper case and the other part of the
	 * string lower case.
	 * 
	 * @param text text to be capitalized
	 * @return text capitalized
	 */
	private String capitalizeFirstLetter(final String text) {
		if (text == null || text.length() == 0) {
			throw new IllegalArgumentException();
		}
		return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
	}

	/**
	 * Return a set with all vehicles stored in Garage.
	 * 
	 * @return set of IThreadVehicle with all vehicles stored
	 */
	public IModelVehicle[] getVehicles() {
		IModelVehicle[] a = new IModelVehicle[]{};
		return this.vehicles.values().toArray(a);
	}

	/**
	 * Returns only a vehicle giving its unique name.
	 * 
	 * @param name unique name of the vehicle
	 * @return the vehicle if founded, null if not.
	 */
	public IModelVehicle getVehicle(final String name) {
		if (this.vehicles.containsKey(name)) {
			return this.vehicles.get(name);
		}
		return null;
	}
	
	/**
	 * Given a position, it returns a vehicle if standing in that
	 * position, null otherwise.
	 * 
	 * @param positionToSearch position's coordinates in
	 * 				which method will search
	 * @return vehicle standing in that position if there's one, null otherwise
	 */
	public IModelVehicle getVehicle(final Coordinates positionToSearch) {
		Iterator<IModelVehicle> i = this.vehicles.values().iterator();
		IModelVehicle v = null;
		while (i.hasNext()) {
			v = i.next();
			if (isPointInVehicleArea(v, positionToSearch)) {
				return v;
			}
		}
		return null;
	}
	
	/**
	 * This private method is a submethod of getVehicle by position.
	 * Given a vehicle it check if the given position is inside it's image.
	 * 
	 * @param v vehicle to control
	 * @param positionToControl position to be checked
	 * @return true if the point is inside the image of the vehicle,
	 * 				false otherwise
	 */
	private boolean isPointInVehicleArea(final IModelVehicle v, final Coordinates positionToControl) {
		Double vehicleArea = (double) (v.getSprite().getWidth(null) * v.getSprite().getHeight(null));
		
		/*Calculating area of 4 triangles
		 * between vehicle image angles and the point given */
		Coordinates[] angles = v.getAnglesPosition();
		Double trianglesArea = 0.0;
		for (int i = 0; i < 4; i++) {
			Double temp = (double) 1 / 2 * ((angles[i].getX() - positionToControl.getX()) * (angles[(i + 1) % 4].getY() - angles[i].getY()) - (angles[i].getX() - angles[(i + 1) % 4].getX()) * (positionToControl.getY() - angles[i].getY()));
			trianglesArea += temp < 0 ? -temp : temp;
		}
		
		//If area of 4 triangles is more than area of my vehicle the point is out of it
		if (vehicleArea < trianglesArea) {
			return false;
		}
		return true;
	}
	
	/**
	 * This method start a vehicle's thread given as input,
	 * if it isn't already running.
	 * 
	 * @param v vehicle's thread to be started
	 */
	private void startVehicle(final IModelVehicle v) {
		if (v != null && !v.isRunning()) {
			v.start();
		}
	}
	
	/**
	 * This method start a vehicle's thread, if it isn't already running.
	 * 
	 * @param name unique name of vehicle's model
	 */
	public void startVehicle(final String name) {
		IModelVehicle v = this.getVehicle(name);
		this.startVehicle(v);
	}
	
	/**
	 * This method start all vehicles' thread, if they aren't already running.
	 */
	public void startAllVehicles() {
		for (IModelVehicle v: this.getVehicles()) {
			try {
				v.start();
			} catch (Exception e) {
				Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Vehicle " + v + "cannot be started.");
			}
		}
	}
	
	/**
	 * This method stop a running vehicle's thread, if it is.
	 * Use this method also to pause a vehicle.
	 * 
	 * @param name unique name of vehicle's model
	 */
	public void stopVehicle(final String name) {
		IModelVehicle v = this.getVehicle(name);
		if (v != null && v.isRunning()) {
			v.getThread().terminate();
		}
	}
	
	/**
	 * Stop/pause all vehicle's threads running.
	 */
	public void stopAllVehicles() {
		for (IModelVehicle v: this.getVehicles()) {
			if (v.isRunning()) {
				v.getThread().terminate();
			}
		}
	}
	
	/**
	 * Return name of all vehicles available at instantiation.
	 * 
	 * @return all instantiable vehicles
	 */
	public List<String> getAllVehiclesTypes() {
		LinkedList<String> vehicleList = new LinkedList<>();
		
		vehicleList.add("car");
			
		return vehicleList;
	}
}