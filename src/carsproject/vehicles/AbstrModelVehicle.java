package carsproject.vehicles;

import java.awt.Image;
import java.util.LinkedList;
import java.util.List;

import carsproject.general.Coordinates;
import carsproject.map.Direction;
import carsproject.map.MapTiles;
import carsproject.map.Roads;

/**
 * This class is a possible abstract implementation of the interface IThreadVehicle.
 * It is hardly recommended to extends this class instead of implements
 * IThreadVehicle because this contains a lot of methods' implementations
 * and the needed constructor.
 * To start the thread you have to use start() method instead of run()
 * which is implemented because of Runnable interface.
 * The only abstract methods are run(), start(), stop() and isRunning().
 * 
 * @author Masini Gioele
 */
public abstract class AbstrModelVehicle implements IModelVehicle {
	
	/** Usual speed of the vehicle. Setted as 50. */
	public static final Double STANDARD_SPEED = 50.0;

	/*		 VARIABILES 		*/
	/**Position of the vehicle on the map.*/
	private Coordinates position;
	/**Speed in km/h, there are some methods then to convert this value in m/s and pixels/timeSlice.*/
	private Double speed;
	/**Speed to be reach in km/h, used when a vehicle has to speed up or to slow down.*/
	private Double speedToReach;
	/**Curvature radius of the vehicle, will assume values between 0 and Math.PI because it is in radians.*/
	private Double curvatureAngleActual;
	/** Curvature that the vehicle have to reach to finish a curve/other movement.
	 * To curve you have to set this variable and then use moveCurve().*/
	private Double curvatureAngleToReach;
	/**Name of the vehicle, have to be unique and Garage will assure that.*/
	private String name;
	/**Reference to thread that move this vehicle.*/
	private IThreadVehicle thread;
	/**Image that represent this vehicle on map.*/
	private Image sprite;
	/**Pixels that the vehicle can still move in a time slice, when it reach 0 stop to move for this frame.*/
	private Double pixelsToGo;
	/** List of occupied tiles. */
	private List<Roads> tilesOccupied = new LinkedList<>();

	/**
	 * Base constructor, need only a unique name, the other
	 * parameters are set as default. Use setters if you
	 * want them to be different.
	 * 
	 * @param name unique name of the vehicle
	 */
	public AbstrModelVehicle(final String name) {
		this.setPosition(new Coordinates(0, 0));
		this.setSpeed(STANDARD_SPEED);
		this.setCurvatureAngleActual(0);
		this.setSprite(null);
		this.setName(name);
	}


	/*			HEART'S METHODS 			*/

	/**
	 * This method must be used to start the thread. This because we want
	 * the possibility to have a vehicle not already in the map or paused.
	 * If thread is already running this method will do nothing.
	 */
	public abstract void start();

	/**
	 * Check if the vehicle is running or not.
	 * 
	 * @return true if running, false if not
	 */
	public abstract boolean isRunning();
		
	
	/*		 CONVERSIONS		*/
	/**
	 * Conversion from km/h to m/s.
	 * 
	 * @param kmPerHour value to be converted
	 * @return the value converted in m/s
	 */
	public static Double kmPerHourToMetersPerSec(final Double kmPerHour) {
		return kmPerHour * 1000 / (60 * 60);
	}
	
	/**
	 * Conversion from m/s to km/h.
	 * 
	 * @param metersPerSecond value to be converted
	 * @return the value converted in km/h
	 */
	public static Double metersPerSecToKmPerHour(final Double metersPerSecond) {
		return metersPerSecond * (60 * 60) / 1000;
	}
	
	/**
	 * Conversion from km/h to pixel/timeSlice.
	 * (unit of measure used in game to update vehicles and manage their speed)
	 * 
	 * @param kmPerHour value to be converted
	 * @return the value converted in pixel/timeSlice
	 */
	public static Double kmPerHoursToPixelPerTimeSlice(final Double kmPerHour) {
		return AbstrModelVehicle.kmPerHourToMetersPerSec(kmPerHour) * MapTiles.PIXEL_ON_METER * IModelVehicle.TIME_SLICE / 1000;
	}
	
	/******** GETTERS AND SETTERS ********/

	/*Position*/
	public Coordinates getPosition() {
		return this.position;
	}

	public void setPosition(final Coordinates position) {
		this.position = position;
	}

	public void setPosition(final Integer x, final Integer y) {
		this.position = new Coordinates(x, y);
	}

	/**
	 * Return center point of the vehicle, calculating it on image dimensions.
	 * 
	 * @return vehicle's center point
	 */
	public Coordinates getCenterPoint() {
		Double angleRad = this.getCurvatureAngleActual();
	    Double cosa = Math.cos(angleRad);
	    Double sina = Math.sin(angleRad);
	    Double width = this.getSprite().getWidth(null) / 2.0;
	    Double height = this.getSprite().getWidth(null) / 2.0;
	    Coordinates pos = this.getPosition();
	    return new Coordinates(pos.getX() + width * cosa - height * sina, pos.getY() + width * sina + height * cosa);
	}
	
	/**
	 * Return an array of 4 elements containing vehicle's angles.
	 * At position 0 there is North-West angle, then the others going clockwise.
	 *
	 * @return array of 4 elements containing coordinates of vehicle's angles
	 */
	public Coordinates[] getAnglesPosition() {
		Coordinates[] angles = new Coordinates[4];
		angles[0] = this.getPosition();
		angles[1] = new Coordinates(this.getPosition().getX() + this.getSprite().getWidth(null) * Math.cos(this.getCurvatureAngleActual()), this.getPosition().getY() + this.getSprite().getWidth(null) * Math.sin(this.getCurvatureAngleActual()));
		angles[3] = new Coordinates(this.getPosition().getX() - this.getSprite().getHeight(null) * Math.sin(this.getCurvatureAngleActual()), this.getPosition().getY() + this.getSprite().getHeight(null) * Math.cos(this.getCurvatureAngleActual()));
		angles[2] = new Coordinates(angles[3].getX() + this.getSprite().getWidth(null) * Math.cos(this.getCurvatureAngleActual()), angles[3].getY() + this.getSprite().getWidth(null) * Math.sin(this.getCurvatureAngleActual()));
		return angles;
	}
	
	/**
	 * Return the direction in which the vehicle is going.
	 * 
	 * @return vehicle's direction
	 */
	public Direction getWay() {
		Double myCurvatureRadians = this.getCurvatureAngleActual();
		Double myCurvatureDegrees = myCurvatureRadians / Math.PI * 180 % 360;
		
		return AbstrModelVehicle.degreesToDirection(myCurvatureDegrees);
	}
	
	/**
	 * Conversion method from Direction to rotational degrees.
	 * 
	 * @param d Direction to be converted
	 * @return equivalent degrees
	 */
	public static Double directionToDegrees(final Direction d) {
		if (d.equals(Direction.E)) {
			return 0d;
		} else if (d.equals(Direction.S)) {
			return 90d;
		} else if (d.equals(Direction.W)) {
			return 180d;
		} else if (d.equals(Direction.N)) {
			return 270d;
		}
		
		return null;
	}

	/**
	 * Conversion method from rotational degrees to Direction
	 * 
	 * @param degrees Value to be converted
	 * @return equivalent direction
	 */
	public static Direction degreesToDirection(final Double degrees) {
		Double myCurvatureDegrees = degrees;
		
		if (myCurvatureDegrees.equals(0d) || myCurvatureDegrees.equals(-0d)) {
			return Direction.E;
		} else if (myCurvatureDegrees.equals(90d) || myCurvatureDegrees.equals(-270d)) {
			return Direction.S;
		} else if (myCurvatureDegrees.equals(180d) || myCurvatureDegrees.equals(-180d)) {
			return Direction.W;
		} else if (myCurvatureDegrees.equals(270d) || myCurvatureDegrees.equals(-90d)) {
			return Direction.N;
		}
		
		return null;
	}
	
	public Double getSpeed() {
		if (this.speed == null) {
			return STANDARD_SPEED;
		}
		return this.speed;
	}

	public void setSpeed(final Double kmh) {
		this.speed = kmh;		
	}
	
	/*Speed to reach, in km/h*/
	public Double getSpeedToReach() {
		if (this.speedToReach == null) {
			return this.getSpeed();
		}
		return this.speedToReach;
	}

	public void setSpeedToReach(final Double kmh) {
		this.speedToReach = kmh;		
	}
	
	/*Sprite image*/
	public Image getSprite() {
		return this.sprite;
	}

	public void setSprite(final Image image) {
		this.sprite = image;
	}
	
	/*Actual curvature*/
	public void setCurvatureAngleActual(final double curvatureAngle) {
		this.curvatureAngleActual = curvatureAngle;
	}
	
	public void setCurvatureAngleActualInDegrees(final double curvatureAngle) {
		this.curvatureAngleActual = Math.toRadians(curvatureAngle);
	}
	
	public double getCurvatureAngleActual() {
		return this.curvatureAngleActual;
	}
	
	public double getCurvatureAngleActualInDegrees() {
		return Math.toDegrees(this.curvatureAngleActual);
	}
	
	/*Curvature angle to reach, used in curves to know then the vehicle has finished to curve or have to continue*/
	public void setCurvatureAngleToReach(final double curvatureAngle) {
		this.curvatureAngleToReach = curvatureAngle;
	}
	
	public double getCurvatureAngleToReach() {
		return this.curvatureAngleToReach != null ? this.curvatureAngleToReach : this.curvatureAngleActual;
	}

	/*Vehicle name*/
	public String getName() {
		return this.name;
	}

	/**
	 * Set vehicle name. This is really important because vehicles got unique names and you can retrieve
	 * one of the from its name.
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/*Thread reference*/
	public IThreadVehicle getThread() {
		return this.thread;
	}
	
	public void setThread(final IThreadVehicle thread) {
		this.thread = thread;
	}
	
	/*Pixels that the vehicle can travel in a time slice*/
	public Double getPixelsToGo() {
		return this.pixelsToGo;
	}
	
	public void setPixelsToGo(Double pixels) {
		this.pixelsToGo = pixels;
	}
	
	public void addOccupiedTile(Roads t) {
		if (t != null && !this.isOccupiedTile(t)) {
			if (t.isOccupied() && t.getOccupyingVehicle().equals(this.getName())) {
				this.tilesOccupied.add(t);
			}
		}
	}
	
	public void removeOccupiedTile(Roads t) {
		if (t != null && this.isOccupiedTile(t)) {
			this.tilesOccupied.remove(t);
		}
	}
	
	public boolean isOccupiedTile(Roads t) {
		return this.tilesOccupied.contains(t);
	}
	
	public List<Roads> getOccupiedTiles() {
		return this.tilesOccupied;
	}

	/*ToString() method, use the form "[threadID:vehicleName] (x,y) is moving at _speed_ km/h;" */
	@Override
	public String toString() {
		return "[" + this.getThread() + ":" + this.getName() + "] " + this.getPosition() + " This vehicle is moving at " + this.getSpeed() + " km/h;";
	}
}
