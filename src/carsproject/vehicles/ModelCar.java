package carsproject.vehicles;

import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;

import carsproject.general.Amanuensis;
import carsproject.general.Coordinates;

/**
 * This is a finished implementation of IThreadVehicle,
 * made extending AbstrThreadVehicle.
 * This contains implementation of abstract methods
 * run(), start(), stop() and isRunning(). 
 * 
 * @author Masini Gioele
 */
public class ModelCar extends AbstrModelVehicle {
	/** Default path to retrieve default image of cars. */
	public static final String DEFAULT_SPRITE_PATH = "/images/default_car";
	/** Extension used for sprites. **/
	public static final String DEFAULT_SPRITE_EXTENSION = ".png";
	private static final Integer NUMBER_OF_SPRITES = 4;
	
	public ModelCar(final String name) {
		super(name);
	}
	

	/**
	 * Start a new thread that update vehicle every {@link IThreadVehicle.TIME_SLICE} fraction of second.
	 * 
	 * @throws IllegalStateException Thread cannot be instantiate if vehicle is positioned out of map
	 */
	public void start() throws IllegalStateException {
		if (!this.isRunning()) {
			if (!Coordinates.isLegal(this.getPosition())) {
				Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Cannot start thread of " + this + " vehicle. Positioned out of map.");
				throw new IllegalStateException() {
					private static final long serialVersionUID = 1L;
					public String getMessage() {
						return "Vehicle model cannot be instantiated because positioned out of map.";
					}
				};
			}
			this.setThread(new ThreadCar(this));
			this.getThread().start();
		}
	}

	/**
	 * Check if thread is running or not and return true if it is running, false otherwise.
	 * 
	 * @return true if it is running, false otherwise
	 */
	public boolean isRunning() {
		if (this.getThread() != null) {
			return true;
		}
		return false;
	}
	
	@Override
	/**
	 * Return sprite associated at this car. If there is no one, return default sprite (default_car.png).
	 * 
	 * @return sprite associated at car
	 */
	public Image getSprite() {
		Image i = super.getSprite();
		if (i == null) {
			URL url = null;
			String imageFullPath = null;
			//If no sprite is set, the program choose random one
			Integer rand = Math.round(Math.round(Math.random() * (ModelCar.NUMBER_OF_SPRITES - 1)));
			imageFullPath = ModelCar.DEFAULT_SPRITE_PATH + rand + ModelCar.DEFAULT_SPRITE_EXTENSION;
			url = ModelCar.class.getResource(imageFullPath);
			i = new ImageIcon(url).getImage();
			this.setSprite(i);
			return i;
		}
		return super.getSprite();
	}
}
