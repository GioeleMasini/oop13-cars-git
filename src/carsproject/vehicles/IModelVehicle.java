package carsproject.vehicles;

import java.awt.Image;
import java.util.List;

import carsproject.general.Coordinates;
import carsproject.map.Direction;
import carsproject.map.Roads;

/**
 * Interface class for vehicles.
 * It contains methods to be implemented in every thread vehicle class.
 * An important and recommended abstract implementation is in
 * AbstrThreadVehicle, class that implements most of those methods.
 * It is really important for every implementation class to have a constructor
 * with "String name" to set vehicle unique name.
 * To start the thread you have to use start() method instead of run()
 * which is implemented because of Runnable interface.
 *
 * ATTENTION: every implementation of this method must be named in the form
 * "Thread____" with name-type of the vehicle as second word.
 * Examples: "ThreadCar", "ThreadScooter", ...
 *
 * @author Masini Gioele
 */
public interface IModelVehicle {
	/**
	 * Number of frames to be shown every second.
	 */
	public static final Integer NUM_FRAMES = 30;
	
	/**
	 * Time fraction used to update vehicles positions, in milliseconds.
	 * (It is set using that formula: milliseconds in a second/frames per second)
	 */
	public static final Integer TIME_SLICE = 1000 / NUM_FRAMES;
	
	/**
	 * Start a new thread that update vehicle every
	 * {@link IThreadVehicle.TIME_SLICE} fraction of second.
	 * 
	 * @throws IllegalStateException Thread cannot be instantiate if vehicle is positioned out of map
	 */
	public void start() throws IllegalStateException;
	
	/**
	 * Returns a boolean flag at true if this thread is running, false if it is stopped.
	 * 
	 * @return true if thread is running, otherwise false
	 */
	public boolean isRunning();
	
	/**
	 * Returns vehicle coordinates.
	 * 
	 * @return coordinates of the vehicle
	 */
	public Coordinates getPosition();
	
	/**
	 * Set vehicle coordinates.
	 * 
	 * @param position coordinates of the vehicle
	 */
	public void setPosition(Coordinates position);
	
	/**
	 * Set vehicle coordinates.
	 * 
	 * @param x value of coordinate x
	 * @param y value of coordinate y
	 */
	public void setPosition(Integer x, Integer y);
	
	/**
	 * Return the direction in which the vehicle is going.
	 * 
	 * @return vehicle's direction
	 */
	public Direction getWay();
	
	/**
	 * Return angles position in a vector of four elements.
	 * 
	 * @return vector of angles' coordinates
	 */
	public Coordinates[] getAnglesPosition();
	
	/**
	 * Return vehicle speed, in Km/h. It can be converted with conversion methods of this class.
	 * 
	 * @return vehicle speed
	 */
	public Double getSpeed();
	
	/**
	 * Set vehicle speed, in Km/h, it could be negative, if doing reverse.
	 * 
	 * @param kmh
	 */
	public void setSpeed(Double kmh);
	
	/**
	 * Returns sprite image of vehicle. From here you can also retrieve vehicles dimensions.
	 * 
	 * @return image sprite of the vehicle
	 */
	public Image getSprite();
	
	/**
	 * Set sprite image of vehicle.
	 * 
	 * @param image image to be setted as sprite of the vehicle
	 */
	public void setSprite(Image image);
	
	/**
	 * Returns the name, it is unique between all vehicles.
	 * 
	 * @return vehicle's name
	 */
	public String getName();
	
	/**
	 * Set vehicle's name, it must be unique.
	 * 
	 * @param name unique name of the vehicle
	 */
	public void setName(String name);
	
	/**
	 * Return curvature radius, in radians.
	 * 
	 * @return curvature radius
	 */
	public double getCurvatureAngleActual();

	/**
	 * Set curvature radius, in radians.
	 * 
	 * @param curvatureAngle rotation radians of vehicle
	 */
	public void setCurvatureAngleActual(double curvatureAngle);
	
	/**
	 * Return curvature radius, in radians.
	 * 
	 * @return rotation radians of vehicle
	 */
	public double getCurvatureAngleToReach();

	/**
	 * Set curvature radius to be reach from the vehicle, in radians.
	 * It is used to know when vehicle as finished to curve.
	 * 
	 * @param curvatureAngle rotation radians to be reach
	 */
	public void setCurvatureAngleToReach(double curvatureAngle);
	
	/**
	 * Return thread of this vehicle.
	 * 
	 * @return thread of this vehicle
	 */
	public IThreadVehicle getThread();

	/**
	 * Set reference to thread updating this vehicle.
	 * 
	 * @param thread thread reference
	 */
	public void setThread(IThreadVehicle thread);
	
	/**
	 * Return pixels that vehicle can still move.
	 * 
	 * @return pixels that this vehicle can move
	 */
	public Double getPixelsToGo();
	
	/**
	 * Set pixels that car can move in that fraction of time.
	 * 
	 * @param pixels pixels this car can move in this fraction of time
	 */
	public void setPixelsToGo(Double pixels);
	
	/**
	 * Set speed to be reached at the end of its actual movement.
	 * 
	 * @param speed speed value to be reached
	 */
	public void setSpeedToReach(Double speed);
	
	/**
	 * Return speed that vehicle will reach at the end its actual a movement.
	 * 
	 * @return speed of vehicle at the end of its actual movement.
	 */
	public Double getSpeedToReach();
	
	/**
	 * Add input road to occupied ones.
	 * Tile passed as input must be already occupied by this vehicle.
	 * 
	 * @param t Occupied tile to add
	 */
	public void addOccupiedTile(Roads t);
	
	/**
	 * Remove an occupied tile.
	 * 
	 * @param t Occupied tile to remove
	 */
	public void removeOccupiedTile(Roads t);
	
	/**
	 * Return true if input tile is occupied, false otherwise.
	 * 
	 * @param t Tile to check
	 * @return true if input tile is occupied, false otherwise
	 */
	public boolean isOccupiedTile(Roads t);
	
	/**
	 * Get list of occupied tiles.
	 * 
	 * @return list of occupied tiles
	 */
	public List<Roads> getOccupiedTiles();
}
