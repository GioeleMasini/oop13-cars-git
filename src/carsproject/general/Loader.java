package carsproject.general;

import carsproject.view.GUIdrawer;

/**
 *  Class used to load the program.
 *
 *  @author Everyone
 */
public final class Loader {

	/**	
	 * Main of the application, start the GUI.
	 * 
	 * @param args	*UNUSED*
	 */
	public static void main(final String[] args) {
		new GUIdrawer();
	}
	
	/** Hidden constructor **/
	private Loader() { };

}
