package carsproject.general;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This class is used as log storer and saver.
 * It divide all text in logs, everyone with a unique name
 * (Examples: "Thread-1", "General logs", "Thread-Pippo", "Map errors", ...)
 * Every log is created when you try to add a line giving as input
 * an uninstantiated log's name.
 * This method got a default save path which is "logs/",
 * but you can choose at save moment a personalized one.
 * 
 * You can't delete a line or a log list, because they are designed to be used
 * as important feedbacks so you have to be able to save them in every moment.
 * But ATTENTION! There is not an auto-save method, so every not-saved change
 * will be lost when you close the program or start another session of
 * simulation with clear() method.
 * 
 * @author Masini Gioele
 */
public final class Amanuensis {
	/** Unique instance of Amanuensis class (Singleton pattern). */
	private static final Amanuensis SINGLETON = new Amanuensis();

	/** Default path to store logs. */
	private static final String DEFAULT_FILE_PATH = "logs/";
	
	/** Name of default error's log. */
	public static final String ERRORS_LOG = "Errors";
	
	/** Storage of all logs, it got a name as first parameter (key)
	 * and a list of strings (lines of text) as value.
	 */
	private Map<String, List<String>> logs = new HashMap<>();
	
	/** Suppresses default constructor, ensuring not-instantiability. */
	private Amanuensis() {
	}
	
	/**
	 * Unique method to get the singleton instance of Amanuensis class.
	 * 
	 * @return	the singleton instance of Amanuensis class
	 */
	public static Amanuensis getInstance() {
		return Amanuensis.SINGLETON;
	}
	
	/**
	 * Return the map of all logs stored in amanuensis.
	 * 
	 * @return map of all logs stored
	 */
	public Map<String, List<String>> getAllLogs() {
		return this.logs;
	}
	
	/**
	 * Add a line of text in "logName" log.
	 * 
	 * @param logName name of the log
	 * @param text text to be add
	 */
	public void addAtLog(final String logName, final String text) {
		if (!this.logs.containsKey(logName)) {
			this.logs.put(logName, new LinkedList<String>());
		}
		this.logs.get(logName).add(text);
	}
	
	/**
	 * Returns list of log's lines if stored in Amanuensis logs.
	 * 
	 * @param 	logName name of the log
	 * @return	null if there isn't the log passed as parameter, list of log's lines if Amanuensis contains it.
	 */
	public List<String> getLog(final String logName) {
		if (this.getAllLogs().containsKey(logName)) {
			return this.getAllLogs().get(logName);
		}
		return null;
	}
	
	/**
	 * Save all logs in a file, with default path (logs/).
	 */
	public void saveAllLogs() {
		Iterator<String> i = this.getAllLogs().keySet().iterator();
		while (i.hasNext()) {
			this.saveLog(i.next());
		}
	}
	
	/**
	 * Save all logs in a file, with path passed as parameter.
	 * 
	 * @param filePath absolute or relative path where logs will be saved
	 */
	public void saveAllLogs(final String filePath) {
		Iterator<String> i = this.getAllLogs().keySet().iterator();
		while (i.hasNext()) {
			this.saveLog(i.next(), filePath);
		}
	}
	
	/**
	 * Save only a log passed as parameter, with default path (logs/).
	 * 
	 * @param logName	Name of log to be saved
	 */
	public void saveLog(final String logName) {
		this.saveLog(logName, Amanuensis.DEFAULT_FILE_PATH);
	}
	
	/**
	 * Save only a log passed as parameter, with path passed in input.
	 * 
	 * @param logName	Name of log to be saved
	 * @param dirPath	Path of the directory where method will save the log (either relative or absolute)
	 */
	public void saveLog(final String logName, final String dirPath) {
		/* Using try-with-resources statement. It assure that a resource will be closed at the end of the statement.
		 * Required Java 7 */
		String filePath = dirPath + logName + ".txt";
		try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(filePath, false)))) {
			Iterator<String> i = this.logs.get(logName).iterator();
			while (i.hasNext()) {
				writer.println(i.next());
			}
		} catch (IOException e) {
			Amanuensis.getInstance().addAtLog(Amanuensis.ERRORS_LOG, "Impossible to write file at position: " + filePath);
			System.err.println("Impossible to write file at position: " + filePath);
		}
	}
	
	/**
	 * ATTENTION! This method will clear every log stored in Amanuensis!
	 * You can't restore them later so be sure to have saved every important log before use that method.
	 * This is the only method to clear logs.
	 */
	public void clearAllLogs() {
		this.logs.clear();
	}

}
